package com.hellooop.odj.authorize.utils;

/**
 *
 * @author lanping
 * @version 1.0
 * @date 2019/12/2
 */
public class Result<T> {
    private boolean success;
    private String code;
    private String msg;
    private T result;

    public Result(boolean success,String msg){
        this.success = success;
        this.msg = msg;
    }

    public Result(boolean success){
        this.success = success;
    }

    public Result(){
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }
}
