package com.hellooop.odj.authorize.utils;

/**
 * 定义常量类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-27
 */
public class ConstantUtil {

	/**
     * config文件路径
     */
    public final static String CONFIG_FILE_PATH = "/config/config.properties";

    /**
     * db文件路径
     */
    public final static String DB_FILE_PATH = "/db/db.properties";
    
    /**
     * redis文件路径
     */
    public final static String REDIS_FILE_PATH = "/redis/redis.properties";
    
    /**
     * 数据有效状态 Y
     */
    public final static String IS_VALID_Y = "Y";

    /**
     * 数据有效状态 N
     */
    public final static String IS_VALID_N = "N";

    /**
     * 模块根节点ID
     */
    public static final String DEFAULT_ROOT_PARANT_ID="-1";

    /**
     * 模块根节点ID
     */
    public static final String DEFAULT_MODULE_ROOT_ID="0";

    /**
     * redis缓存的存用户模块 key的后缀
     */
    public final static String REDIS_KEY_USER_MODULES_SUFFX="_roles_modules";


    /**
     * redis缓存所有模块
     */
    public final static String REDIS_KEY_ALL_MODULE_MENUS_SUFFX="_all_module_menus";


    /**
     * redis缓存权限模块
     */
    public final static String REDIS_KEY_PERMISSION_MODULE_MENUS_SUFFX="_permission_set_module_menus";


    public enum moduleType{

        module_link("module_link","链接"),
        module_word("module_word","文字"),
        module_function("module_function","方法"),
        module_window("module_window","弹窗");

        private String code;
        private String name;

        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
        public String getCode() {
            return code;
        }
        public void setCode(String code) {
            this.code = code;
        }

        private moduleType(String code,String name) {
            this.name = name;
            this.code = code;
        }
    }

    public static String getModuleType(String moduleCode){
        String moduleType = null;
        for(ConstantUtil.moduleType type :ConstantUtil.moduleType.values()){
            if(type.getCode().equals(moduleCode)){
                moduleType = type.getName();
                break;
            }
        }
        return moduleType;
    }

}