package com.hellooop.odj.authorize.utils;

import java.util.Properties;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class RedisUtils {
    private static JedisPool jedisPool = null;

    /**
     * 初始化Redis连接池
     */
    static {
        try {
            Properties properties = PropertiesUtil.getProperties(ConstantUtil.REDIS_FILE_PATH);
            String host = properties.getProperty("redis.host");
            int port = Integer.parseInt(properties.getProperty("redis.port"));
            int maxActive = Integer.parseInt(properties.getProperty("redis.maxActive"));
            int maxIdle = Integer.parseInt(properties.getProperty("redis.maxIdle"));
            int maxWait = Integer.parseInt(properties.getProperty("redis.maxWait"));
            int timeout = Integer.parseInt(properties.getProperty("redis.timeout"));
            String testOnBorrow = properties.getProperty("redis.testOnBorrow");
            String password = properties.getProperty("redis.password");
            password = StringUtil.isNotNull(password)?password:null;
            int database = Integer.parseInt(properties.getProperty("redis.database"));

            JedisPoolConfig config = new JedisPoolConfig();
            config.setMaxTotal(maxActive);
            config.setMaxIdle(maxIdle);
            config.setMaxWaitMillis(maxWait);
            config.setTestOnBorrow("true".equals(testOnBorrow) ? true : false);
            jedisPool = new JedisPool(config, host, port, timeout, password, database);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取Jedis实例
     */

    public synchronized static Jedis getJedis() {
        try {
            if (jedisPool != null) {
                Jedis resource = jedisPool.getResource();
                return resource;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /***
     *
     * 释放资源
     */
    @SuppressWarnings("deprecation")
    public synchronized static void releaseConn(final Jedis jedis) {
        if (jedis != null) {
            jedisPool.returnResource(jedis);
        }
    }
}

