package com.hellooop.odj.authorize.bean;

import com.hellooop.odj.authorize.bean.po.SysModule;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 权限
 * @author lanping
 * @version 1.0
 * @date 2019-11-30 下午1:50:42
 */
@ApiModel(value = "模块信息子类",description ="模块信息子类" )
public class Module extends SysModule{

    @ApiModelProperty(value = "上级模块名称（非实体属性，仅查询显示）")
    private String parentModuleName;

    @ApiModelProperty(value = "模块类型名称（非实体属性，仅查询显示）")
    private String moduleTypeName;

    public String getParentModuleName() {
        return parentModuleName;
    }

    public void setParentModuleName(String parentModuleName) {
        this.parentModuleName = parentModuleName;
    }

    public String getModuleTypeName() {
        return moduleTypeName;
    }

    public void setModuleTypeName(String moduleTypeName) {
        this.moduleTypeName = moduleTypeName;
    }
}
