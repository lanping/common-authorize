package com.hellooop.odj.authorize.bean.po;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 模块关联业务信息实体类
 * @author lanping
 * @version 1.0
 * @date 2019-11-29
 */
@ApiModel(value = "模块关联业务信息",description = "模块关联业务信息")
public class SysModuleLink { 

	@ApiModelProperty(value = "主键ID")
	private String linkId;

	@ApiModelProperty(value = "模块ID")
	private String moduleId;

	@ApiModelProperty(value = "业务关联ID 角色ID或用户ID")
	private String busiId;

	@ApiModelProperty(value = "有效状态Y有效，Y无效")
	private String state;

	public String getLinkId() {
		return linkId;
	}

	public void setLinkId(String linkId) { 
		this.linkId = linkId;
	}

	public String getModuleId() {
		return moduleId;
	}

	public void setModuleId(String moduleId) { 
		this.moduleId = moduleId;
	}

	public String getBusiId() {
		return busiId;
	}

	public void setBusiId(String busiId) { 
		this.busiId = busiId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) { 
		this.state = state;
	}

	@Override
	public String toString() {
		return "SysModuleLink [linkId=" + linkId + ",moduleId=" + moduleId + ",busiId=" + busiId + ",state=" + state + "]";
	}
}