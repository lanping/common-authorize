package com.hellooop.odj.authorize.bean.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 模块类型
 * @author lanping
 * @version 1.0
 * @date 2019/12/7
 */
@ApiModel(value = "功能模块类型",description = "功能模块类型")
public class ModuleType {

    @ApiModelProperty(value = "编号")
    private String code;

    @ApiModelProperty(value = "名称")
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
