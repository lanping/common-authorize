package com.hellooop.odj.authorize.bean.po;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 系统模块信息实体类
 * @author lanping
 * @version 1.0
 * @date 2019-11-27
 */
@ApiModel(value = "系统模块信息",description = "系统模块信息")
public class SysModule {

	@ApiModelProperty(value = "主键ID")
	private String moduleId;

	@ApiModelProperty(value = "系统编号")
	private String sysCode;

	@ApiModelProperty(value = "模块名称")
	private String moduleName;

	@ApiModelProperty(value = "模块编号")
	private String moduleCode;

	@ApiModelProperty(value = "排序号",example = "1")
	private Integer rank;

	@ApiModelProperty(value = "上级模块ID")
	private String parentId;

	@ApiModelProperty(value = "操作地址")
	private String moduleUrl;

	@ApiModelProperty(value = "备注")
	private String remark;

	@ApiModelProperty(value = "模块类型")
	private String moduleType;

	@ApiModelProperty(value = "图标")
	private String icon;

	@ApiModelProperty(value = "是否菜单 Y 是 N否")
	private String menuStatus;

	@ApiModelProperty(value = "默认模块状态  Y 是 N 否 ")
	private String defaultStatus;

	@ApiModelProperty(value = "是否显示状态 Y 是 N 否 ")
	private String showStatus;

	@ApiModelProperty(value = "有效状态Y，有效，N无效")
	private String state;

	@ApiModelProperty(value = "菜单级别")
	private Integer moduleLevel;

	@ApiModelProperty(value = "备用字段")
	private String memo;

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Integer getModuleLevel() {
		return moduleLevel;
	}

	public void setModuleLevel(Integer moduleLevel) {
		this.moduleLevel = moduleLevel;
	}

	public String getModuleId() {
		return moduleId;
	}

	public void setModuleId(String moduleId) { 
		this.moduleId = moduleId;
	}

	public String getSysCode() {
		return sysCode;
	}

	public void setSysCode(String sysCode) { 
		this.sysCode = sysCode;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) { 
		this.moduleName = moduleName;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) { 
		this.moduleCode = moduleCode;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) { 
		this.rank = rank;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) { 
		this.parentId = parentId;
	}

	public String getModuleUrl() {
		return moduleUrl;
	}

	public void setModuleUrl(String moduleUrl) { 
		this.moduleUrl = moduleUrl;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) { 
		this.remark = remark;
	}

	public String getModuleType() {
		return moduleType;
	}

	public void setModuleType(String moduleType) { 
		this.moduleType = moduleType;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) { 
		this.icon = icon;
	}

	public String getMenuStatus() {
		return menuStatus;
	}

	public void setMenuStatus(String menuStatus) { 
		this.menuStatus = menuStatus;
	}

	public String getDefaultStatus() {
		return defaultStatus;
	}

	public void setDefaultStatus(String defaultStatus) { 
		this.defaultStatus = defaultStatus;
	}

	public String getShowStatus() {
		return showStatus;
	}

	public void setShowStatus(String showStatus) { 
		this.showStatus = showStatus;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) { 
		this.state = state;
	}

	@Override
	public String toString() {
		return "SysModule [moduleId=" + moduleId + ",sysCode=" + sysCode + ",moduleName=" + moduleName + ",moduleCode=" + moduleCode + ",rank=" + rank + ",parentId=" + parentId + ",moduleUrl=" + moduleUrl + ",remark=" + remark + ",moduleType=" + moduleType + ",icon=" + icon + ",menuStatus=" + menuStatus + ",defaultStatus=" + defaultStatus + ",showStatus=" + showStatus + ",state=" + state + "]";
	}
}