package com.hellooop.odj.authorize.bean.vo;

import com.hellooop.odj.authorize.utils.StringUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 功能模块树形结构 菜单数据
 * @author lanping
 * @version 1.0
 * @date 2019-11-30 下午1:25:28
 */
@ApiModel(value = "功能模块树形对象",description = "功能模块树形对象")
public class ModuleTreeVo{

	@ApiModelProperty(value = "模块ID")
	private String id;	//ID

	@ApiModelProperty(value = "模块ID")
	private String key;	//ID

	@ApiModelProperty(value = "上级模块ID")
	private String parentId;

	@ApiModelProperty(value = "模块名称")
	private String title; //名称

	@ApiModelProperty(value = "模块编号")
	private String field; // 字典值

	@ApiModelProperty(value = "图标")
	private String icon;	//图标

	@ApiModelProperty(value = "是否选中")
	private boolean checked; //是否选中

	@ApiModelProperty(value = "是否选中")
	private boolean selectable; //是否选中

	@ApiModelProperty(value = "是否展开")
	private boolean spread = true;	//是否展开

	@ApiModelProperty(value = "是否禁用")
	private boolean disabled;	//是否禁用

	@ApiModelProperty(value = "禁掉 checkbox")
	private boolean disableCheckbox;

	@ApiModelProperty(value = "跳转地址")
	private String href;	//跳转地址

	@ApiModelProperty(value = "模块类型编号")
	private String type;	//模块类型

	@ApiModelProperty(value = "是否菜单 Y 是 N否")
	private String menuStatus; //是否菜单 Y 是 N否

	@ApiModelProperty(value = "默认菜单状态 Y 是 N否")
	private String defaultStatus;	//默认菜单状态 Y 是 N否

	@ApiModelProperty(value = "是否显示 Y 是 N否")
	private String showStatus;	//是否显示  Y 是 N否

	@ApiModelProperty(value = "备注")
	private String remark; //备注

	@ApiModelProperty(value = "排序号")
	private Integer rank = 0; //排序号

	@ApiModelProperty(value = "子集")
	private List<ModuleTreeVo> children;	//子集

	@ApiModelProperty(value = "菜单级别")
	private Integer moduleLevel=0;	//级别

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getKey() {
		if(StringUtil.isNull(key)){
			return this.id;
		}
		return key;
	}

	public Integer getModuleLevel() {
		return moduleLevel;
	}

	public void setModuleLevel(Integer moduleLevel) {
		this.moduleLevel = moduleLevel;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public boolean isSelectable() {
		return checked;
	}

	public void setSelectable(boolean selectable) {
		this.selectable = selectable;
	}

	public boolean isDisableCheckbox() {
		return disableCheckbox;
	}

	public void setDisableCheckbox(boolean disableCheckbox) {
		this.disableCheckbox = disableCheckbox;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getMenuStatus() {
		return menuStatus;
	}

	public void setMenuStatus(String menuStatus) {
		this.menuStatus = menuStatus;
	}

	public String getDefaultStatus() {
		return defaultStatus;
	}
	public void setDefaultStatus(String defaultStatus) {
		this.defaultStatus = defaultStatus;
	}
	public String getShowStatus() {
		return showStatus;
	}
	public void setShowStatus(String showStatus) {
		this.showStatus = showStatus;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public boolean isDisabled() {
		return disabled;
	}
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	public boolean isSpread() {
		return spread;
	}
	public void setSpread(boolean spread) {
		this.spread = spread;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public List<ModuleTreeVo> getChildren() {
		return children;
	}
	public void setChildren(List<ModuleTreeVo> children) {
		this.children = children;
	}

	@Override
	public String toString() {
		return "ModuleTreeVo [id=" + id + ", title=" + title + ", field="
				+ field + ", icon=" + icon + ", checked=" + checked
				+ ", spread=" + spread + ", disabled=" + disabled + ", href="
				+ href + ", type=" + type + ", menuStatus=" + menuStatus
				+ ", defaultStatus=" + defaultStatus + ", showStatus="
				+ showStatus + ", children=" + children + "]";
	}

}
