package com.hellooop.odj.authorize.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.hellooop.odj.authorize.dao.BaseDao;
import com.hellooop.odj.authorize.dao.ITransaction;
import com.hellooop.odj.authorize.dao.SysModuleLinkDao;
import com.hellooop.odj.authorize.bean.po.SysModuleLink;
import com.hellooop.odj.authorize.utils.DaoException;

import com.hellooop.odj.authorize.utils.StringUtil;

/**
 * 模块关联业务信息实现类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-27
 */
public class SysModuleLinkDaoImpl extends BaseDao implements SysModuleLinkDao {

	/**
	 * 新增
	 * 
	 * @param sysModuleLink
	 * @return
	 * @throws DaoException
	 */
	public int insertSysModuleLink(SysModuleLink sysModuleLink) throws DaoException {
		try {
			StringBuffer sb = new StringBuffer();
			List<Object> params = new ArrayList<Object>();
			sb.append("insert into sys_module_link ( ");
			if (StringUtil.isNotNull(sysModuleLink.getLinkId())) {
				sb.append("link_id , ");
			}
			if (StringUtil.isNotNull(sysModuleLink.getModuleId())) {
				sb.append("module_id , ");
			}
			if (StringUtil.isNotNull(sysModuleLink.getBusiId())) {
				sb.append("busi_id , ");
			}
			if (StringUtil.isNotNull(sysModuleLink.getState())) {
				sb.append("state , ");
			}
			if (sb.indexOf(",") != -1) {
				sb.deleteCharAt(sb.lastIndexOf(","));
			}
			sb.append(" ) values ( ");
			if (StringUtil.isNotNull(sysModuleLink.getLinkId())) {
				sb.append(" ? , ");
				params.add(sysModuleLink.getLinkId());
			}
			if (StringUtil.isNotNull(sysModuleLink.getModuleId())) {
				sb.append(" ? , ");
				params.add(sysModuleLink.getModuleId());
			}
			if (StringUtil.isNotNull(sysModuleLink.getBusiId())) {
				sb.append(" ? , ");
				params.add(sysModuleLink.getBusiId());
			}
			if (StringUtil.isNotNull(sysModuleLink.getState())) {
				sb.append(" ? , ");
				params.add(sysModuleLink.getState());
			}
			if (sb.indexOf(",") != -1) {
				sb.deleteCharAt(sb.lastIndexOf(","));
			}
			sb.append(" ) ");
			System.out.println(sb.toString());
			return singleUpdate(sb.toString(), params);
		} catch (Exception e) {
			throw new DaoException(e,"新增SysModuleLink时异常...");
		}
	}

	/**
	 * 新增
	 * 
	 * @param sysModuleLink
	 * @return
	 * @throws DaoException
	 */
	public int insertSysModuleLink(SysModuleLink sysModuleLink, ITransaction tran) throws DaoException {
		try {
			StringBuffer sb = new StringBuffer();
			List<Object> params = new ArrayList<Object>();
			sb.append("insert into sys_module_link ( ");
			if (StringUtil.isNotNull(sysModuleLink.getLinkId())) {
				sb.append("link_id , ");
			}
			if (StringUtil.isNotNull(sysModuleLink.getModuleId())) {
				sb.append("module_id , ");
			}
			if (StringUtil.isNotNull(sysModuleLink.getBusiId())) {
				sb.append("busi_id , ");
			}
			if (StringUtil.isNotNull(sysModuleLink.getState())) {
				sb.append("state , ");
			}
			if (sb.indexOf(",") != -1) {
				sb.deleteCharAt(sb.lastIndexOf(","));
			}
			sb.append(" ) values ( ");
			if (StringUtil.isNotNull(sysModuleLink.getLinkId())) {
				sb.append(" ? , ");
				params.add(sysModuleLink.getLinkId());
			}
			if (StringUtil.isNotNull(sysModuleLink.getModuleId())) {
				sb.append(" ? , ");
				params.add(sysModuleLink.getModuleId());
			}
			if (StringUtil.isNotNull(sysModuleLink.getBusiId())) {
				sb.append(" ? , ");
				params.add(sysModuleLink.getBusiId());
			}
			if (StringUtil.isNotNull(sysModuleLink.getState())) {
				sb.append(" ? , ");
				params.add(sysModuleLink.getState());
			}
			if (sb.indexOf(",") != -1) {
				sb.deleteCharAt(sb.lastIndexOf(","));
			}
			sb.append(" ) ");
			System.out.println(sb.toString());
			return singleUpdate(sb.toString(), params, tran.getConnection());
		} catch (Exception e) {
			throw new DaoException(e,"新增SysModuleLink时异常...");
		}
	}

	/**
	 * 修改
	 * 
	 * @param SysModuleLink
	 * @return
	 * @throws DaoException
	 */
	public int updateSysModuleLink(SysModuleLink sysModuleLink) throws DaoException {
		try {
			StringBuffer sb = new StringBuffer();
			List<Object> params = new ArrayList<Object>();
			sb.append("update sys_module_link set ");
			if (StringUtil.isNotNull(sysModuleLink.getModuleId())) {
				sb.append("module_id = ? , ");
				params.add(sysModuleLink.getModuleId());
			}
			if (StringUtil.isNotNull(sysModuleLink.getBusiId())) {
				sb.append("busi_id = ? , ");
				params.add(sysModuleLink.getBusiId());
			}
			if (StringUtil.isNotNull(sysModuleLink.getState())) {
				sb.append("state = ? , ");
				params.add(sysModuleLink.getState());
			}
			if (sb.indexOf(",") != -1) {
				sb.deleteCharAt(sb.lastIndexOf(","));
			}
			sb.append(" where link_id = ? ");
			params.add(sysModuleLink.getLinkId());
			System.out.println(sb.toString());
			return singleUpdate(sb.toString(), params);
		} catch (Exception e) {
			throw new DaoException(e,"修改SysModuleLink时异常...");
		}
	}

	/**
	 * 修改
	 * 
	 * @param sysModuleLink
	 * @return
	 * @throws DaoException
	 */
	public int updateSysModuleLink(SysModuleLink sysModuleLink, ITransaction tran) throws DaoException {
		try {
			StringBuffer sb = new StringBuffer();
			List<Object> params = new ArrayList<Object>();
			sb.append("update sys_module_link set ");
			if (StringUtil.isNotNull(sysModuleLink.getModuleId())) {
				sb.append("module_id = ? , ");
				params.add(sysModuleLink.getModuleId());
			}
			if (StringUtil.isNotNull(sysModuleLink.getBusiId())) {
				sb.append("busi_id = ? , ");
				params.add(sysModuleLink.getBusiId());
			}
			if (StringUtil.isNotNull(sysModuleLink.getState())) {
				sb.append("state = ? , ");
				params.add(sysModuleLink.getState());
			}
			if (sb.indexOf(",") != -1) {
				sb.deleteCharAt(sb.lastIndexOf(","));
			}
			sb.append(" where link_id = ? ");
			params.add(sysModuleLink.getLinkId());
			System.out.println(sb.toString());
			return singleUpdate(sb.toString(), params, tran.getConnection());
		} catch (Exception e) {
			throw new DaoException(e,"修改SysModuleLink时异常...");
		}
	}

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteSysModuleLink(Map<String, Object> map) throws DaoException {
		try {
			String sql = "delete from sys_module_link where 1=1 ";

			List<Object> params = new ArrayList<Object>();
			if(map.get("linkIds")!=null){
				sql+=" and link_id in (?) ";
				params.add(map.get("linkIds"));
			}
			if(map.get("busiId")!=null){
				sql+=" and busi_id = ? ";
				params.add(map.get("busiId"));
			}
			System.out.println(sql);

			return singleUpdate(sql,params);
		} catch (Exception e) {
			throw new DaoException(e,"删除SysModuleLink时异常...");
		}
	}

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteSysModuleLink(Map<String, Object> map, ITransaction tran) throws DaoException {
		try {
			String sql = "delete from sys_module_link where 1=1 ";

			List<Object> params = new ArrayList<Object>();
			if(map.get("linkIds")!=null){
				sql+=" and link_id in ("+map.get("linkIds")+") ";
			}
			if(map.get("busiId")!=null){
				sql+=" and busi_id = ? ";
				params.add(map.get("busiId"));
			}
			System.out.println(sql);
			return singleUpdate(sql, params, tran.getConnection());
		} catch (Exception e) {
			throw new DaoException(e,"删除SysModuleLink时异常...");
		}
	}

	/**
	 * 查询单个
	 * 
	 * @param linkId
	 * @return
	 * @throws DaoException
	 */
	public SysModuleLink selectSysModuleLinkByLinkId(String linkId) throws DaoException {
		try {
			String sql="select * from sys_module_link where link_id = ? ";
			List<Object> params = new ArrayList<Object>();
			params.add(linkId);
			System.out.println(sql);
			List<SysModuleLink> list = findSingleTable(sql, params, SysModuleLink.class);
			if (list != null && list.size() > 0) {
				return list.get(0);
			}
			return null;
		} catch (Exception e) {
			throw new DaoException(e,"查询单个SysModuleLink时异常...");
		}
	}

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<SysModuleLink> selectSysModuleLinkList(Map<String, Object> map) throws DaoException {
		try {
			List<Object> params = new ArrayList<Object>();
			String sql = "select * from sys_module_link where 1=1 ";
			if (map.get("linkId") != null) {
				sql += " and link_id =? ";
				params.add(map.get("linkId"));
			}
			if (map.get("moduleId") != null) {
				sql += " and module_id =? ";
				params.add(map.get("moduleId"));
			}
			if (map.get("busiId") != null) {
				sql += " and busi_id =? ";
				params.add(map.get("busiId"));
			}
			if (map.get("state") != null) {
				sql += " and state =? ";
				params.add(map.get("state"));
			}
			if (map.get("sortName") != null) {
				sql += " order by "+ map.get("sortName");
			}
			if (map.get("sortOrder") != null) {
				sql += " "+map.get("sortOrder") ;
			}
			if(map!=null && StringUtil.isNotNull(map.get("pageIndex")) && StringUtil.isNotNull(map.get("pageSize"))){
				Integer index = Integer.parseInt(map.get("pageIndex").toString());
				Integer size = Integer.parseInt(map.get("pageSize").toString());
				Integer count = (index-1)*size;
				sql+=" LIMIT "+count+","+size;
			}
			System.out.println(sql);
			return findSingleTable(sql, params, SysModuleLink.class);
		} catch (Exception e) {
			throw new DaoException(e,"分页查询SysModuleLink时异常...");
		}
	}

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<SysModuleLink> selectAllSysModuleLinkList(Map<String, Object> map) throws DaoException {
		try {
			String sql = "select * from sys_module_link where 1=1 ";
			List<Object> params = new ArrayList<Object>();
			if (map.get("linkId") != null) {
				sql += " and link_id =? ";
				params.add(map.get("linkId"));
			}
			if (map.get("moduleId") != null) {
				sql += " and module_id =? ";
				params.add(map.get("moduleId"));
			}
			if (map.get("busiId") != null) {
				sql += " and busi_id =? ";
				params.add(map.get("busiId"));
			}
			if (map.get("state") != null) {
				sql += " and state =? ";
				params.add(map.get("state"));
			}
			if (map.get("sortName") != null) {
				sql += " order by "+ map.get("sortName");
			}
			if (map.get("sortOrder") != null) {
				sql += " "+map.get("sortOrder") ;
			}
			System.out.println(sql);
			return findSingleTable(sql, params, SysModuleLink.class);
		} catch (Exception e) {
			throw new DaoException(e,"查询所有SysModuleLink时异常...");
		}
	}

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectSysModuleLinkListCount(Map<String, Object> map) throws DaoException {
		try {
			String sql = "select count(1) from sys_module_link where 1=1 ";
			List<Object> params = new ArrayList<Object>();
			if (map.get("linkId") != null) {
				sql += " and link_id =? ";
				params.add(map.get("linkId"));
			}
			if (map.get("moduleId") != null) {
				sql += " and module_id =? ";
				params.add(map.get("moduleId"));
			}
			if (map.get("busiId") != null) {
				sql += " and busi_id =? ";
				params.add(map.get("busiId"));
			}
			if (map.get("state") != null) {
				sql += " and state =? ";
				params.add(map.get("state"));
			}
			System.out.println(sql);
			List<String> list = uniqueResult(sql, params);
			if (list != null && list.size() > 0) {
				return Integer.parseInt(list.get(0).toString());
			}
			return 0;
		} catch (Exception e) {
			throw new DaoException(e,"查询SysModuleLink总数时异常...");
		}
	}

}