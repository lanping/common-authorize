package com.hellooop.odj.authorize.dao.impl;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.hellooop.odj.authorize.bean.Module;
import com.hellooop.odj.authorize.bean.po.SysModule;
import com.hellooop.odj.authorize.bean.vo.ModuleTreeVo;
import com.hellooop.odj.authorize.dao.BaseDao;
import com.hellooop.odj.authorize.dao.ITransaction;
import com.hellooop.odj.authorize.dao.SysModuleDao;
import com.hellooop.odj.authorize.utils.DaoException;
import com.hellooop.odj.authorize.utils.StringUtil;

/**
 * 系统模块信息实现类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-27
 */
public class SysModuleDaoImpl extends BaseDao implements SysModuleDao {

	/**
	 * 新增
	 * 
	 * @param sysModule
	 * @return
	 * @throws DaoException
	 */
	public int insertSysModule(SysModule sysModule) throws DaoException {
		try {
			StringBuffer sb = new StringBuffer();
			List<Object> params = new ArrayList<Object>();
			sb.append("insert into sys_module ( ");
			if (StringUtil.isNotNull(sysModule.getModuleId())) {
				sb.append("module_id , ");
			}
			if (StringUtil.isNotNull(sysModule.getSysCode())) {
				sb.append("sys_code , ");
			}
			if (StringUtil.isNotNull(sysModule.getModuleName())) {
				sb.append("module_name , ");
			}
			if (StringUtil.isNotNull(sysModule.getModuleCode())) {
				sb.append("module_code , ");
			}
			if (StringUtil.isNotNull(sysModule.getRank())) {
				sb.append("rank , ");
			}
			if (StringUtil.isNotNull(sysModule.getParentId())) {
				sb.append("parent_id , ");
			}
			if (StringUtil.isNotNull(sysModule.getModuleUrl())) {
				sb.append("module_url , ");
			}
			if (StringUtil.isNotNull(sysModule.getRemark())) {
				sb.append("remark , ");
			}
			if (StringUtil.isNotNull(sysModule.getModuleType())) {
				sb.append("module_type , ");
			}
			if (StringUtil.isNotNull(sysModule.getIcon())) {
				sb.append("icon , ");
			}
			if (StringUtil.isNotNull(sysModule.getMenuStatus())) {
				sb.append("menu_status , ");
			}
			if (StringUtil.isNotNull(sysModule.getDefaultStatus())) {
				sb.append("default_status , ");
			}
			if (StringUtil.isNotNull(sysModule.getShowStatus())) {
				sb.append("show_status , ");
			}
			if (StringUtil.isNotNull(sysModule.getState())) {
				sb.append("state , ");
			}
			if (StringUtil.isNotNull(sysModule.getMemo())) {
				sb.append("memo , ");
			}
			if (StringUtil.isNotNull(sysModule.getModuleLevel())) {
				sb.append("module_level , ");
			}
			if (sb.indexOf(",") != -1) {
				sb.deleteCharAt(sb.lastIndexOf(","));
			}
			sb.append(" ) values ( ");
			if (StringUtil.isNotNull(sysModule.getModuleId())) {
				sb.append(" ? , ");
				params.add(sysModule.getModuleId());
			}
			if (StringUtil.isNotNull(sysModule.getSysCode())) {
				sb.append(" ? , ");
				params.add(sysModule.getSysCode());
			}
			if (StringUtil.isNotNull(sysModule.getModuleName())) {
				sb.append(" ? , ");
				params.add(sysModule.getModuleName());
			}
			if (StringUtil.isNotNull(sysModule.getModuleCode())) {
				sb.append(" ? , ");
				params.add(sysModule.getModuleCode());
			}
			if (StringUtil.isNotNull(sysModule.getRank())) {
				sb.append(" ? , ");
				params.add(sysModule.getRank());
			}
			if (StringUtil.isNotNull(sysModule.getParentId())) {
				sb.append(" ? , ");
				params.add(sysModule.getParentId());
			}
			if (StringUtil.isNotNull(sysModule.getModuleUrl())) {
				sb.append(" ? , ");
				params.add(sysModule.getModuleUrl());
			}
			if (StringUtil.isNotNull(sysModule.getRemark())) {
				sb.append(" ? , ");
				params.add(sysModule.getRemark());
			}
			if (StringUtil.isNotNull(sysModule.getModuleType())) {
				sb.append(" ? , ");
				params.add(sysModule.getModuleType());
			}
			if (StringUtil.isNotNull(sysModule.getIcon())) {
				sb.append(" ? , ");
				params.add(sysModule.getIcon());
			}
			if (StringUtil.isNotNull(sysModule.getMenuStatus())) {
				sb.append(" ? , ");
				params.add(sysModule.getMenuStatus());
			}
			if (StringUtil.isNotNull(sysModule.getDefaultStatus())) {
				sb.append(" ? , ");
				params.add(sysModule.getDefaultStatus());
			}
			if (StringUtil.isNotNull(sysModule.getShowStatus())) {
				sb.append(" ? , ");
				params.add(sysModule.getShowStatus());
			}
			if (StringUtil.isNotNull(sysModule.getState())) {
				sb.append(" ? , ");
				params.add(sysModule.getState());
			}
			if (StringUtil.isNotNull(sysModule.getMemo())) {
				sb.append(" ? , ");
				params.add(sysModule.getMemo());
			}
			if (StringUtil.isNotNull(sysModule.getModuleLevel())) {
				sb.append(" ? , ");
				params.add(sysModule.getModuleLevel());
			}
			if (sb.indexOf(",") != -1) {
				sb.deleteCharAt(sb.lastIndexOf(","));
			}
			sb.append(" ) ");
			System.out.println(sb.toString());
			return singleUpdate(sb.toString(), params);
		} catch (Exception e) {
			throw new DaoException(e,"新增SysModule时异常...");
		}
	}

	/**
	 * 新增
	 * 
	 * @param sysModule
	 * @return
	 * @throws DaoException
	 */
	public int insertSysModule(SysModule sysModule, ITransaction tran) throws DaoException {
		try {
			StringBuffer sb = new StringBuffer();
			List<Object> params = new ArrayList<Object>();
			sb.append("insert into sys_module ( ");
			if (StringUtil.isNotNull(sysModule.getModuleId())) {
				sb.append("module_id , ");
			}
			if (StringUtil.isNotNull(sysModule.getSysCode())) {
				sb.append("sys_code , ");
			}
			if (StringUtil.isNotNull(sysModule.getModuleName())) {
				sb.append("module_name , ");
			}
			if (StringUtil.isNotNull(sysModule.getModuleCode())) {
				sb.append("module_code , ");
			}
			if (StringUtil.isNotNull(sysModule.getRank())) {
				sb.append("rank , ");
			}
			if (StringUtil.isNotNull(sysModule.getParentId())) {
				sb.append("parent_id , ");
			}
			if (StringUtil.isNotNull(sysModule.getModuleUrl())) {
				sb.append("module_url , ");
			}
			if (StringUtil.isNotNull(sysModule.getRemark())) {
				sb.append("remark , ");
			}
			if (StringUtil.isNotNull(sysModule.getModuleType())) {
				sb.append("module_type , ");
			}
			if (StringUtil.isNotNull(sysModule.getIcon())) {
				sb.append("icon , ");
			}
			if (StringUtil.isNotNull(sysModule.getMenuStatus())) {
				sb.append("menu_status , ");
			}
			if (StringUtil.isNotNull(sysModule.getDefaultStatus())) {
				sb.append("default_status , ");
			}
			if (StringUtil.isNotNull(sysModule.getShowStatus())) {
				sb.append("show_status , ");
			}
			if (StringUtil.isNotNull(sysModule.getState())) {
				sb.append("state , ");
			}
			if (StringUtil.isNotNull(sysModule.getMemo())) {
				sb.append("memo , ");
			}
			if (StringUtil.isNotNull(sysModule.getModuleLevel())) {
				sb.append("module_level , ");
			}
			if (sb.indexOf(",") != -1) {
				sb.deleteCharAt(sb.lastIndexOf(","));
			}
			sb.append(" ) values ( ");
			if (StringUtil.isNotNull(sysModule.getModuleId())) {
				sb.append(" ? , ");
				params.add(sysModule.getModuleId());
			}
			if (StringUtil.isNotNull(sysModule.getSysCode())) {
				sb.append(" ? , ");
				params.add(sysModule.getSysCode());
			}
			if (StringUtil.isNotNull(sysModule.getModuleName())) {
				sb.append(" ? , ");
				params.add(sysModule.getModuleName());
			}
			if (StringUtil.isNotNull(sysModule.getModuleCode())) {
				sb.append(" ? , ");
				params.add(sysModule.getModuleCode());
			}
			if (StringUtil.isNotNull(sysModule.getRank())) {
				sb.append(" ? , ");
				params.add(sysModule.getRank());
			}
			if (StringUtil.isNotNull(sysModule.getParentId())) {
				sb.append(" ? , ");
				params.add(sysModule.getParentId());
			}
			if (StringUtil.isNotNull(sysModule.getModuleUrl())) {
				sb.append(" ? , ");
				params.add(sysModule.getModuleUrl());
			}
			if (StringUtil.isNotNull(sysModule.getRemark())) {
				sb.append(" ? , ");
				params.add(sysModule.getRemark());
			}
			if (StringUtil.isNotNull(sysModule.getModuleType())) {
				sb.append(" ? , ");
				params.add(sysModule.getModuleType());
			}
			if (StringUtil.isNotNull(sysModule.getIcon())) {
				sb.append(" ? , ");
				params.add(sysModule.getIcon());
			}
			if (StringUtil.isNotNull(sysModule.getMenuStatus())) {
				sb.append(" ? , ");
				params.add(sysModule.getMenuStatus());
			}
			if (StringUtil.isNotNull(sysModule.getDefaultStatus())) {
				sb.append(" ? , ");
				params.add(sysModule.getDefaultStatus());
			}
			if (StringUtil.isNotNull(sysModule.getShowStatus())) {
				sb.append(" ? , ");
				params.add(sysModule.getShowStatus());
			}
			if (StringUtil.isNotNull(sysModule.getState())) {
				sb.append(" ? , ");
				params.add(sysModule.getState());
			}
			if (StringUtil.isNotNull(sysModule.getMemo())) {
				sb.append(" ? , ");
				params.add(sysModule.getMemo());
			}
			if (StringUtil.isNotNull(sysModule.getModuleLevel())) {
				sb.append(" ? , ");
				params.add(sysModule.getModuleLevel());
			}
			if (sb.indexOf(",") != -1) {
				sb.deleteCharAt(sb.lastIndexOf(","));
			}
			sb.append(" ) ");
			System.out.println(sb.toString());
			return singleUpdate(sb.toString(), params, tran.getConnection());
		} catch (Exception e) {
			throw new DaoException(e,"新增SysModule时异常...");
		}
	}

	/**
	 * 修改
	 * 
	 * @param sysModule
	 * @return
	 * @throws DaoException
	 */
	public int updateSysModule(SysModule sysModule) throws DaoException {
		try {
			StringBuffer sb = new StringBuffer();
			List<Object> params = new ArrayList<Object>();
			sb.append("update sys_module set ");
			if (StringUtil.isNotNull(sysModule.getSysCode())) {
				sb.append("sys_code = ? , ");
				params.add(sysModule.getSysCode());
			}
			if (StringUtil.isNotNull(sysModule.getModuleName())) {
				sb.append("module_name = ? , ");
				params.add(sysModule.getModuleName());
			}
			if (StringUtil.isNotNull(sysModule.getModuleCode())) {
				sb.append("module_code = ? , ");
				params.add(sysModule.getModuleCode());
			}
			if (StringUtil.isNotNull(sysModule.getRank())) {
				sb.append("rank = ? , ");
				params.add(sysModule.getRank());
			}
			if (StringUtil.isNotNull(sysModule.getParentId())) {
				sb.append("parent_id = ? , ");
				params.add(sysModule.getParentId());
			}
			if (StringUtil.isNotNull(sysModule.getModuleUrl())) {
				sb.append("module_url = ? , ");
				params.add(sysModule.getModuleUrl());
			}
			if (StringUtil.isNotNull(sysModule.getRemark())) {
				sb.append("remark = ? , ");
				params.add(sysModule.getRemark());
			}
			if (StringUtil.isNotNull(sysModule.getModuleType())) {
				sb.append("module_type = ? , ");
				params.add(sysModule.getModuleType());
			}
			if (StringUtil.isNotNull(sysModule.getIcon())) {
				sb.append("icon = ? , ");
				params.add(sysModule.getIcon());
			}
			if (StringUtil.isNotNull(sysModule.getMenuStatus())) {
				sb.append("menu_status = ? , ");
				params.add(sysModule.getMenuStatus());
			}
			if (StringUtil.isNotNull(sysModule.getDefaultStatus())) {
				sb.append("default_status = ? , ");
				params.add(sysModule.getDefaultStatus());
			}
			if (StringUtil.isNotNull(sysModule.getShowStatus())) {
				sb.append("show_status = ? , ");
				params.add(sysModule.getShowStatus());
			}
			if (StringUtil.isNotNull(sysModule.getState())) {
				sb.append("state = ? , ");
				params.add(sysModule.getState());
			}
			if (StringUtil.isNotNull(sysModule.getMemo())) {
				sb.append("memo=? , ");
				params.add(sysModule.getMemo());
			}
			if (StringUtil.isNotNull(sysModule.getModuleLevel())) {
				sb.append("module_level=? , ");
				params.add(sysModule.getModuleLevel());
			}
			if (sb.indexOf(",") != -1) {
				sb.deleteCharAt(sb.lastIndexOf(","));
			}
			sb.append(" where module_id = ? ");
			params.add(sysModule.getModuleId());
			System.out.println(sb.toString());
			return singleUpdate(sb.toString(), params);
		} catch (Exception e) {
			throw new DaoException(e,"修改SysModule时异常...");
		}
	}

	/**
	 * 修改
	 * 
	 * @param sysModule
	 * @return
	 * @throws DaoException
	 */
	public int updateSysModule(SysModule sysModule, ITransaction tran) throws DaoException {
		try {
			StringBuffer sb = new StringBuffer();
			List<Object> params = new ArrayList<Object>();
			sb.append("update sys_module set ");
			if (StringUtil.isNotNull(sysModule.getSysCode())) {
				sb.append("sys_code = ? , ");
				params.add(sysModule.getSysCode());
			}
			if (StringUtil.isNotNull(sysModule.getModuleName())) {
				sb.append("module_name = ? , ");
				params.add(sysModule.getModuleName());
			}
			if (StringUtil.isNotNull(sysModule.getModuleCode())) {
				sb.append("module_code = ? , ");
				params.add(sysModule.getModuleCode());
			}
			if (StringUtil.isNotNull(sysModule.getRank())) {
				sb.append("rank = ? , ");
				params.add(sysModule.getRank());
			}
			if (StringUtil.isNotNull(sysModule.getParentId())) {
				sb.append("parent_id = ? , ");
				params.add(sysModule.getParentId());
			}
			if (StringUtil.isNotNull(sysModule.getModuleUrl())) {
				sb.append("module_url = ? , ");
				params.add(sysModule.getModuleUrl());
			}
			if (StringUtil.isNotNull(sysModule.getRemark())) {
				sb.append("remark = ? , ");
				params.add(sysModule.getRemark());
			}
			if (StringUtil.isNotNull(sysModule.getModuleType())) {
				sb.append("module_type = ? , ");
				params.add(sysModule.getModuleType());
			}
			if (StringUtil.isNotNull(sysModule.getIcon())) {
				sb.append("icon = ? , ");
				params.add(sysModule.getIcon());
			}
			if (StringUtil.isNotNull(sysModule.getMenuStatus())) {
				sb.append("menu_status = ? , ");
				params.add(sysModule.getMenuStatus());
			}
			if (StringUtil.isNotNull(sysModule.getDefaultStatus())) {
				sb.append("default_status = ? , ");
				params.add(sysModule.getDefaultStatus());
			}
			if (StringUtil.isNotNull(sysModule.getShowStatus())) {
				sb.append("show_status = ? , ");
				params.add(sysModule.getShowStatus());
			}
			if (StringUtil.isNotNull(sysModule.getState())) {
				sb.append("state = ? , ");
				params.add(sysModule.getState());
			}
			if (StringUtil.isNotNull(sysModule.getMemo())) {
				sb.append("memo=? , ");
				params.add(sysModule.getMemo());
			}
			if (StringUtil.isNotNull(sysModule.getModuleLevel())) {
				sb.append("module_level=? , ");
				params.add(sysModule.getModuleLevel());
			}
			if (sb.indexOf(",") != -1) {
				sb.deleteCharAt(sb.lastIndexOf(","));
			}
			sb.append(" where module_id = ? ");
			params.add(sysModule.getModuleId());
			System.out.println(sb.toString());
			return singleUpdate(sb.toString(), params, tran.getConnection());
		} catch (Exception e) {
			throw new DaoException(e,"修改SysModule时异常...");
		}
	}

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteSysModule(Map<String, Object> map) throws DaoException {
		try {
			String sql = "delete from sys_module where module_id in ( "+map.get("moduleIds")+" ) ";
			System.out.println(sql);
			return singleUpdate(sql,null);
		} catch (Exception e) {
			throw new DaoException(e,"删除SysModule时异常...");
		}
	}

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteSysModule(Map<String, Object> map, ITransaction tran) throws DaoException {
		try {
			String sql = "delete from sys_module where module_id in ( "+map.get("moduleIds")+" ) ";
			System.out.println(sql);
			return singleUpdate(sql, null, tran.getConnection());
		} catch (Exception e) {
			throw new DaoException(e,"删除SysModule时异常...");
		}
	}

	/**
	 * 查询单个
	 * 
	 * @param moduleId
	 * @return
	 * @throws DaoException
	 */
	public Module selectSysModuleByModuleId(String moduleId) throws DaoException {
		try {
			String sql="select module.*,parent.module_name as parent_module_name " +
					"	from sys_module module LEFT JOIN sys_module parent on parent.module_id = module.parent_id" +
					" where module.module_id = ? ";
			List<Object> params = new ArrayList<Object>();
			params.add(moduleId);
			System.out.println(sql);
			List<Module> list = findSingleTable(sql, params, Module.class);
			if (list != null && list.size() > 0) {
				return list.get(0);
			}
			return null;
		} catch (Exception e) {
			throw new DaoException(e,"查询单个SysModule时异常...");
		}
	}

	/**
	 * 查询单个
	 *
	 * @param moduleCode
	 * @return
	 * @throws DaoException
	 */
	public Module selectSysModuleByModuleCode(String moduleCode) throws DaoException{
		try {
			String sql="select module.*,parent.module_name as parent_module_name " +
					"	from sys_module module LEFT JOIN sys_module parent on parent.module_id = module.parent_id" +
					" where module.module_code = ? ";
			List<Object> params = new ArrayList<Object>();
			params.add(moduleCode);
			System.out.println(sql);
			List<Module> list = findSingleTable(sql, params, Module.class);
			if (list != null && list.size() > 0) {
				return list.get(0);
			}
			return null;
		} catch (Exception e) {
			throw new DaoException(e,"查询单个SysModule时异常...");
		}
	}

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<Module> selectSysModuleList(Map<String, Object> map) throws DaoException {
		try {
			List<Object> params = new ArrayList<Object>();
			String sql="select module.*,parent.module_name as parent_module_name" +
					" from sys_module module LEFT JOIN sys_module parent on parent.module_id = module.parent_id" +
					" where 1=1 ";
			if (map.get("moduleId") != null) {
				sql += " and module.module_id =? ";
				params.add(map.get("moduleId"));
			}
			if (map.get("moduleIds") != null) {
				sql += " and module.module_id in ("+map.get("moduleIds")+") ";
			}
			if (map.get("noModuleIds") != null) {
				sql += " and module.module_id not in ("+map.get("noModuleIds")+") ";
			}
			if (map.get("sysCode") != null) {
				sql += " and module.sys_code =? ";
				params.add(map.get("sysCode"));
			}
			if (map.get("moduleName") != null) {
                sql += " and module.module_name like '%"+map.get("moduleName")+"%' ";
			}
			if (map.get("moduleCode") != null) {
				sql += " and module.module_code =? ";
				params.add(map.get("moduleCode"));
			}
			if (map.get("checkModuleUrl") != null) {
				sql +=" and ? like CONCAT('%',module.module_ur)";
				params.add(map.get("checkModuleUrl"));
			}
			if (map.get("rank") != null) {
				sql += " and module.rank =? ";
				params.add(map.get("rank"));
			}
			if (map.get("parentId") != null) {
				sql += " and module.parent_id =? ";
				params.add(map.get("parentId"));
			}
			if (map.get("moduleUrl") != null) {
				sql += " and module.module_url =? ";
				params.add(map.get("moduleUrl"));
			}
			if (map.get("remark") != null) {
				sql += " and module.remark =? ";
				params.add(map.get("remark"));
			}
			if (map.get("moduleType") != null) {
				sql += " and module.module_type =? ";
				params.add(map.get("moduleType"));
			}
			if (map.get("icon") != null) {
				sql += " and module.icon =? ";
				params.add(map.get("icon"));
			}
			if (map.get("menuStatus") != null) {
				sql += " and module.menu_status =? ";
				params.add(map.get("menuStatus"));
			}
			if (map.get("defaultStatus") != null) {
				sql += " and module.default_status =? ";
				params.add(map.get("defaultStatus"));
			}
			if (map.get("showStatus") != null) {
				sql += " and module.show_status =? ";
				params.add(map.get("showStatus"));
			}
			if (map.get("state") != null) {
				sql += " and module.state =? ";
				params.add(map.get("state"));
			}
			if (map.get("memo") != null) {
				sql += " and module.memo =? ";
				params.add(map.get("memo"));
			}
			if (map.get("moduleLevel") != null) {
				sql += " and module.module_level =? ";
				params.add(map.get("moduleLevel"));
			}
			if (map.get("sortName") != null) {
				sql += " order by module."+ map.get("sortName");
			}
			if (map.get("sortOrder") != null) {
				sql += " "+map.get("sortOrder") ;
			}
			if(map!=null && StringUtil.isNotNull(map.get("pageIndex")) && StringUtil.isNotNull(map.get("pageSize"))){
				Integer index = Integer.parseInt(map.get("pageIndex").toString());
				Integer size = Integer.parseInt(map.get("pageSize").toString());
				Integer count = (index-1)*size;
				sql+=" LIMIT "+count+","+size;
			}
			System.out.println(sql);
			return findSingleTable(sql, params, Module.class);
		} catch (Exception e) {
			throw new DaoException(e,"分页查询SysModule时异常...");
		}
	}
	
	
	/**
	 * 查询用户模块树形集合
	 */
	public List<ModuleTreeVo> selectUserModuleTreeVoList(Connection conn,Map<String, Object> map)
			throws DaoException{
		try {
			String sql = "select * from ( select * from ( ";
			sql+= "select module.module_id as id,module.module_name as title," +
					" module.module_code as field,module.icon as icon," +
					" module.module_url as href,module.module_type as type," +
					" module.menu_status,module.default_status,module.show_status,module.remark,module.parent_id,module.rank,module.module_level " +
					" from sys_module module " +
					" INNER JOIN sys_module_link moduleLink on moduleLink.module_id = module.module_id and moduleLink.busi_id in ("+map.get("busiIds")+")" +
					" where 1=1 ";
			List<Object> params = new ArrayList<Object>();

			if (map.get("sysCode") != null) {
				sql += " and module.sys_code =? ";
				params.add(map.get("sysCode"));
			}

			if (map.get("moduleIds") != null) {
				sql += " and module.module_id in ("+map.get("moduleIds")+") ";
			}
			if (map.get("noModuleIds") != null) {
				sql += " and module.module_id not in ("+map.get("noModuleIds")+") ";
			}

			if (map.get("parentId") != null) {
				sql += " and module.parent_id =? ";
				params.add(map.get("parentId"));
			}

			if (map.get("parentModuleCode") != null) {
				sql += " and module.parent_id in ( " +
						" select module_id from sys_module where module_code = ? and state = 'Y' and show_status = 'Y' ) ";
				params.add(map.get("parentModuleCode"));
			}

			if (map.get("moduleUrl") != null) {
				sql += " and module.module_url =? ";
				params.add(map.get("moduleUrl"));
			}
			if (map.get("moduleType") != null) {
				sql += " and module.module_type =? ";
				params.add(map.get("moduleType"));
			}
			if (map.get("menuStatus") != null) {
				sql += " and module.menu_status =? ";
				params.add(map.get("menuStatus"));
			}

			if (map.get("memo") != null) {
				sql += " and module.memo =? ";
				params.add(map.get("memo"));
			}
			if (map.get("moduleLevel") != null) {
				sql += " and module.module_level =? ";
				params.add(map.get("moduleLevel"));
			}

			sql += " and module.default_status ='N' ";
			sql += " and module.show_status ='Y' ";
			sql += " and module.state ='Y' ";
			
			sql+=" GROUP BY module.module_id,module.module_name,module.module_code," +
					" module.module_url,module.module_type,module.icon," +
					" module.menu_status,module.default_status,module.show_status,module.remark,module.parent_id,module.rank,module.module_level ";
			sql += " order by module.rank ";
			
			sql+=" ) a UNION ALL select * from ( ";
			
			sql+= "select module.module_id as id,module.module_name as title," +
					" module.module_code as field,module.icon as icon," +
					" module.module_url as href,module.module_type as type," +
					" module.menu_status,module.default_status,module.show_status,module.remark,module.parent_id,module.rank,module.module_level " +
					" from sys_module module where 1=1 ";
			
			if (map.get("sysCode") != null) {
				sql += " and module.sys_code =? ";
				params.add(map.get("sysCode"));
			}

			if (map.get("moduleIds") != null) {
				sql += " and module.module_id in ("+map.get("moduleIds")+") ";
			}
			if (map.get("noModuleIds") != null) {
				sql += " and module.module_id not in ("+map.get("noModuleIds")+") ";
			}

			if (map.get("parentId") != null) {
				sql += " and module.parent_id =? ";
				params.add(map.get("parentId"));
			}
			if (map.get("parentModuleCode") != null) {
				sql += " and module.parent_id in ( " +
						" select module_id from sys_module where module_code = ? and state = 'Y' and show_status = 'Y' ) ";
				params.add(map.get("parentModuleCode"));
			}
			if (map.get("moduleUrl") != null) {
				sql += " and module.module_url =? ";
				params.add(map.get("moduleUrl"));
			}
			if (map.get("moduleType") != null) {
				sql += " and module.module_type =? ";
				params.add(map.get("moduleType"));
			}
			if (map.get("menuStatus") != null) {
				sql += " and module.menu_status =? ";
				params.add(map.get("menuStatus"));
			}

			if (map.get("memo") != null) {
				sql += " and module.memo =? ";
				params.add(map.get("memo"));
			}
			if (map.get("moduleLevel") != null) {
				sql += " and module.module_level =? ";
				params.add(map.get("moduleLevel"));
			}

			sql += " and module.default_status ='Y' ";
			sql += " and module.show_status ='Y' ";
			sql += " and module.state ='Y' ";
			sql += " order by module.rank ";

			sql+=" ) b ) a where 1=1  order by rank";
			
			System.out.println(sql);
			return findSingleTable(conn,sql, params, ModuleTreeVo.class);
		} catch (Exception e) {
			throw new DaoException(e,"查询所有SysModule时异常...");
		}
	}
	
	/**
	 * 查询所有模块树形集合
	 */
	public List<ModuleTreeVo> selectAllModuleTreeVoList(Connection conn,Map<String, Object> map)
			throws DaoException{
		try {
			String sql = "select module.module_id as id,module.module_name as title," +
					" module.module_code as field,module.icon as icon," +
					" module.module_url as href,module.module_type as type," +
					" module.menu_status,module.default_status,module.show_status,module.remark,module.parent_id,module.rank,module.module_level " +
					" from sys_module module where 1=1 ";
			List<Object> params = new ArrayList<Object>();
			if (map.get("moduleId") != null) {
				sql += " and module.module_id =? ";
				params.add(map.get("moduleId"));
			}
			if (map.get("moduleIds") != null) {
				sql += " and module.module_id in ("+map.get("moduleIds")+") ";
			}
			if (map.get("noModuleIds") != null) {
				sql += " and module.module_id not in ("+map.get("noModuleIds")+") ";
			}
			if (map.get("sysCode") != null) {
				sql += " and module.sys_code =? ";
				params.add(map.get("sysCode"));
			}
			if (map.get("moduleName") != null) {
                sql += " and module.module_name like '%"+map.get("moduleName")+"%' ";
			}
			if (map.get("moduleCode") != null) {
				sql += " and module.module_code =? ";
				params.add(map.get("moduleCode"));
			}
			if (map.get("rank") != null) {
				sql += " and module.rank =? ";
				params.add(map.get("rank"));
			}
			if (map.get("parentId") != null) {
				sql += " and module.parent_id =? ";
				params.add(map.get("parentId"));
			}
			if (map.get("moduleUrl") != null) {
				sql += " and module.module_url =? ";
				params.add(map.get("moduleUrl"));
			}
			if (map.get("checkModuleUrl") != null) {
				sql +=" and ? like CONCAT('%',module.module_url)";
				params.add(map.get("checkModuleUrl"));
			}
			if (map.get("remark") != null) {
				sql += " and module.remark =? ";
				params.add(map.get("remark"));
			}
			if (map.get("moduleType") != null) {
				sql += " and module.module_type =? ";
				params.add(map.get("moduleType"));
			}
			if (map.get("icon") != null) {
				sql += " and module.icon =? ";
				params.add(map.get("icon"));
			}
			if (map.get("menuStatus") != null) {
				sql += " and module.menu_status =? ";
				params.add(map.get("menuStatus"));
			}
			if (map.get("defaultStatus") != null) {
				sql += " and module.default_status =? ";
				params.add(map.get("defaultStatus"));
			}
			if (map.get("showStatus") != null) {
				sql += " and module.show_status =? ";
				params.add(map.get("showStatus"));
			}
			if (map.get("memo") != null) {
				sql += " and module.memo =? ";
				params.add(map.get("memo"));
			}
			if (map.get("moduleLevel") != null) {
				sql += " and module.module_level =? ";
				params.add(map.get("moduleLevel"));
			}
			sql += " and module.state ='Y' ";
			sql += " order by module.rank ";
			System.out.println(sql);
			return findSingleTable(conn,sql, params, ModuleTreeVo.class);
		} catch (Exception e) {
			throw new DaoException(e,"查询所有SysModule时异常...");
		}
	}

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<Module> selectAllSysModuleList(Map<String, Object> map) throws DaoException {
		try {
			List<Object> params = new ArrayList<Object>();
			String sql="select module.*,parent.module_name as parent_module_name " +
					" from sys_module module LEFT JOIN sys_module parent on parent.module_id = module.parent_id" +
					" where 1=1 ";
			if (map.get("moduleId") != null) {
				sql += " and module.module_id =? ";
				params.add(map.get("moduleId"));
			}
			if (map.get("moduleIds") != null) {
				sql += " and module.module_id in ("+map.get("moduleIds")+") ";
			}
			if (map.get("noModuleIds") != null) {
				sql += " and module.module_id not in ("+map.get("noModuleIds")+") ";
			}
			if (map.get("sysCode") != null) {
				sql += " and module.sys_code =? ";
				params.add(map.get("sysCode"));
			}
			if (map.get("moduleName") != null) {
                sql += " and module.module_name like '%"+map.get("moduleName")+"%' ";
			}
			if (map.get("moduleCode") != null) {
				sql += " and module.module_code =? ";
				params.add(map.get("moduleCode"));
			}
			if (map.get("checkModuleUrl") != null) {
				sql +=" and ? like CONCAT('%',module.module_url)";
				params.add(map.get("checkModuleUrl"));
			}
			if (map.get("rank") != null) {
				sql += " and module.rank =? ";
				params.add(map.get("rank"));
			}
			if (map.get("parentId") != null) {
				sql += " and module.parent_id =? ";
				params.add(map.get("parentId"));
			}
			if (map.get("moduleUrl") != null) {
				sql += " and module.module_url =? ";
				params.add(map.get("moduleUrl"));
			}
			if (map.get("remark") != null) {
				sql += " and module.remark =? ";
				params.add(map.get("remark"));
			}
			if (map.get("moduleType") != null) {
				sql += " and module.module_type =? ";
				params.add(map.get("moduleType"));
			}
			if (map.get("icon") != null) {
				sql += " and module.icon =? ";
				params.add(map.get("icon"));
			}
			if (map.get("menuStatus") != null) {
				sql += " and module.menu_status =? ";
				params.add(map.get("menuStatus"));
			}
			if (map.get("defaultStatus") != null) {
				sql += " and module.default_status =? ";
				params.add(map.get("defaultStatus"));
			}
			if (map.get("showStatus") != null) {
				sql += " and module.show_status =? ";
				params.add(map.get("showStatus"));
			}
			if (map.get("state") != null) {
				sql += " and module.state =? ";
				params.add(map.get("state"));
			}
			if (map.get("memo") != null) {
				sql += " and module.memo =? ";
				params.add(map.get("memo"));
			}
			if (map.get("moduleLevel") != null) {
				sql += " and module.module_level =? ";
				params.add(map.get("moduleLevel"));
			}
			if (map.get("sortName") != null) {
				sql += " order by module."+ map.get("sortName");
			}
			if (map.get("sortOrder") != null) {
				sql += " "+map.get("sortOrder") ;
			}
			System.out.println(sql);
			return findSingleTable(sql, params, Module.class);
		} catch (Exception e) {
			throw new DaoException(e,"查询所有SysModule时异常...");
		}
	}

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectSysModuleListCount(Map<String, Object> map) throws DaoException {
		try {
			String sql = "select count(1) from sys_module where 1=1 ";
			List<Object> params = new ArrayList<Object>();
			if (map.get("moduleId") != null) {
				sql += " and module_id =? ";
				params.add(map.get("moduleId"));
			}
			if (map.get("moduleIds") != null) {
				sql += " and module_id in ("+map.get("moduleIds")+") ";
			}
			if (map.get("noModuleIds") != null) {
				sql += " and module_id not in ("+map.get("noModuleIds")+") ";
			}
			if (map.get("sysCode") != null) {
				sql += " and sys_code =? ";
				params.add(map.get("sysCode"));
			}
			if (map.get("moduleName") != null) {
				sql += " and module_name like '%"+map.get("moduleName")+"%' ";
			}
			if (map.get("moduleCode") != null) {
				sql += " and module_code =? ";
				params.add(map.get("moduleCode"));
			}
			if (map.get("rank") != null) {
				sql += " and rank =? ";
				params.add(map.get("rank"));
			}
			if (map.get("parentId") != null) {
				sql += " and parent_id =? ";
				params.add(map.get("parentId"));
			}
			if (map.get("moduleUrl") != null) {
				sql += " and module_url =? ";
				params.add(map.get("moduleUrl"));
			}
			if (map.get("checkModuleUrl") != null) {
				sql +=" and ? like CONCAT('%',module_url)";
				params.add(map.get("checkModuleUrl"));
			}
			if (map.get("remark") != null) {
				sql += " and remark =? ";
				params.add(map.get("remark"));
			}
			if (map.get("moduleType") != null) {
				sql += " and module_type =? ";
				params.add(map.get("moduleType"));
			}
			if (map.get("icon") != null) {
				sql += " and icon =? ";
				params.add(map.get("icon"));
			}
			if (map.get("menuStatus") != null) {
				sql += " and menu_status =? ";
				params.add(map.get("menuStatus"));
			}
			if (map.get("defaultStatus") != null) {
				sql += " and default_status =? ";
				params.add(map.get("defaultStatus"));
			}
			if (map.get("showStatus") != null) {
				sql += " and show_status =? ";
				params.add(map.get("showStatus"));
			}
			if (map.get("state") != null) {
				sql += " and state =? ";
				params.add(map.get("state"));
			}
			if (map.get("memo") != null) {
				sql += " and memo =? ";
				params.add(map.get("memo"));
			}
			if (map.get("moduleLevel") != null) {
				sql += " and module_level =? ";
				params.add(map.get("moduleLevel"));
			}
			System.out.println(sql);
			List<String> list = uniqueResult(sql, params);
			if (list != null && list.size() > 0) {
				return Integer.parseInt(list.get(0).toString());
			}
			return 0;
		} catch (Exception e) {
			throw new DaoException(e,"查询SysModule总数时异常...");
		}
	}

}