package com.hellooop.odj.authorize.dao;

import java.sql.Connection;
import java.util.List;
import java.util.Map;

import com.hellooop.odj.authorize.bean.Module;
import com.hellooop.odj.authorize.bean.po.SysModule;
import com.hellooop.odj.authorize.bean.vo.ModuleTreeVo;
import com.hellooop.odj.authorize.utils.DaoException;

/**
 * 系统模块信息接口
 * @author lanping
 * @version 1.0
 * @date 2019-11-27
 */
public interface SysModuleDao {

	/**
	 * 新增
	 * 
	 * @param sysModule
	 * @return
	 * @throws DaoException
	 */
	public int insertSysModule(SysModule sysModule) throws DaoException;

	/**
	 * 新增
	 * 
	 * @param sysModule
	 * @return
	 * @throws DaoException
	 */
	public int insertSysModule(SysModule sysModule, ITransaction tran)
			throws DaoException;

	/**
	 * 修改
	 * 
	 * @param sysModule
	 * @return
	 * @throws DaoException
	 */
	public int updateSysModule(SysModule sysModule) throws DaoException;

	/**
	 * 修改
	 * 
	 * @param sysModule
	 * @return
	 * @throws DaoException
	 */
	public int updateSysModule(SysModule sysModule, ITransaction tran)
			throws DaoException;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteSysModule(Map<String, Object> map) throws DaoException;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteSysModule(Map<String, Object> map, ITransaction tran)
			throws DaoException;

	/**
	 * 查询单个
	 * 
	 * @param moduleId
	 * @return
	 * @throws DaoException
	 */
	public Module selectSysModuleByModuleId(String moduleId) throws DaoException;


	/**
	 * 查询单个
	 *
	 * @param moduleCode
	 * @return
	 * @throws DaoException
	 */
	public Module selectSysModuleByModuleCode(String moduleCode) throws DaoException;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<Module> selectSysModuleList(Map<String, Object> map) throws DaoException;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<Module> selectAllSysModuleList(Map<String, Object> map)
			throws DaoException;

	/**
	 * 查询总数
	 *
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectSysModuleListCount(Map<String, Object> map) throws DaoException;

	/**
	 * 查询所有模块树形集合
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<ModuleTreeVo> selectAllModuleTreeVoList(Connection conn,Map<String, Object> map)
			throws DaoException;

	/**
	 * 查询用户模块树形集合
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<ModuleTreeVo> selectUserModuleTreeVoList(Connection conn,Map<String, Object> map)
			throws DaoException;

}