package com.hellooop.odj.authorize.dao;

import java.util.List;
import java.util.Map;

import com.hellooop.odj.authorize.bean.po.SysModuleLink;
import com.hellooop.odj.authorize.utils.DaoException;

/**
 * 模块关联业务信息接口
 * @author lanping
 * @version 1.0
 * @date 2019-11-27
 */
public interface SysModuleLinkDao {

	/**
	 * 新增
	 * 
	 * @param sysModuleLink
	 * @return
	 * @throws DaoException
	 */
	public int insertSysModuleLink(SysModuleLink sysModuleLink) throws DaoException;

	/**
	 * 新增
	 * 
	 * @param sysModuleLink
	 * @return
	 * @throws DaoException
	 */
	public int insertSysModuleLink(SysModuleLink sysModuleLink, ITransaction tran)
			throws DaoException;

	/**
	 * 修改
	 * 
	 * @param sysModuleLink
	 * @return
	 * @throws DaoException
	 */
	public int updateSysModuleLink(SysModuleLink sysModuleLink) throws DaoException;

	/**
	 * 修改
	 * 
	 * @param sysModuleLink
	 * @return
	 * @throws DaoException
	 */
	public int updateSysModuleLink(SysModuleLink sysModuleLink, ITransaction tran)
			throws DaoException;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteSysModuleLink(Map<String, Object> map) throws DaoException;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteSysModuleLink(Map<String, Object> map, ITransaction tran)
			throws DaoException;

	/**
	 * 查询单个
	 * 
	 * @param linkId
	 * @return
	 * @throws DaoException
	 */
	public SysModuleLink selectSysModuleLinkByLinkId(String linkId) throws DaoException;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<SysModuleLink> selectSysModuleLinkList(Map<String, Object> map) throws DaoException;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<SysModuleLink> selectAllSysModuleLinkList(Map<String, Object> map)
			throws DaoException;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectSysModuleLinkListCount(Map<String, Object> map) throws DaoException;

}