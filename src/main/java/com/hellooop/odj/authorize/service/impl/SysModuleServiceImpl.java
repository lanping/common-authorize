package com.hellooop.odj.authorize.service.impl;

import com.hellooop.odj.authorize.bean.Module;
import com.hellooop.odj.authorize.bean.po.SysModule;
import com.hellooop.odj.authorize.bean.vo.ModuleTreeVo;
import com.hellooop.odj.authorize.dao.BaseDao;
import com.hellooop.odj.authorize.dao.SysModuleDao;
import com.hellooop.odj.authorize.dao.impl.SysModuleDaoImpl;
import com.hellooop.odj.authorize.service.SysModuleService;
import com.hellooop.odj.authorize.utils.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * 系统模块基本操作类
 * @author lanping
 * @version 1.0
 * @date 2019/12/7
 */
public class SysModuleServiceImpl implements SysModuleService {

    /**
     * 新增权限
     * @param sysModule
     * @throws DaoException
     */
    public void insertSysModule(SysModule sysModule) throws DaoException, ValidateException {
        SysModuleDao sysModuleDao = new SysModuleDaoImpl();

        String moduleCode = sysModule.getModuleCode();
        if(StringUtil.isNotNull(moduleCode)){
            Map<String,Object> map = new HashMap<String, Object>();
            map.put("moduleCode", moduleCode);
            int count = sysModuleDao.selectSysModuleListCount(map);
            if(count>0){
                throw new ValidateException("模块编号已存在");
            }
        }

        //查询上级信息
        if(StringUtil.isNotNull(sysModule.getParentId())){
            Module module = sysModuleDao.selectSysModuleByModuleId(sysModule.getParentId());
            if(module!=null && StringUtil.isNotNull(module.getModuleLevel())){
                sysModule.setModuleLevel(module.getModuleLevel()+1);
            }
        }

        sysModule.setModuleId(UUID.randomUUID().toString());
        sysModule.setState(ConstantUtil.IS_VALID_Y);
        sysModuleDao.insertSysModule(sysModule);
    }

    /**
     * 修改权限
     * @param sysModule
     * @throws DaoException
     */
    public void updateSysModule(SysModule sysModule) throws DaoException, ValidateException {
        SysModuleDao sysModuleDao = new SysModuleDaoImpl();

        SysModule oldModule = sysModuleDao.selectSysModuleByModuleId(sysModule.getModuleId());
        if(oldModule==null){
            throw new ValidateException("参数错误");
        }

        String moduleCode = sysModule.getModuleCode();
        if(StringUtil.isNotNull(moduleCode)){
            Map<String,Object> map = new HashMap<String, Object>();
            map.put("moduleCode", moduleCode);
            int count = sysModuleDao.selectSysModuleListCount(map);
            if(count>0 && !oldModule.getModuleCode().equals(moduleCode)){
                throw new ValidateException("模块编号已存在");
            }
        }

        //查询上级信息
        if(StringUtil.isNotNull(sysModule.getParentId())){
            Module module = sysModuleDao.selectSysModuleByModuleId(sysModule.getParentId());
            if(module!=null && StringUtil.isNotNull(module.getModuleLevel())){
                sysModule.setModuleLevel(module.getModuleLevel()+1);
            }
        }

        sysModuleDao.updateSysModule(sysModule);
    }

    /**
     * 删除权限
     * @param moduleIds
     * @throws DaoException
     */
    public void deleteSysModule(String ... moduleIds) throws DaoException, ValidateException {
        if(moduleIds==null || moduleIds.length<=0){
            throw new ValidateException("参数错误");
        }

        SysModuleDao sysModuleDao = new SysModuleDaoImpl();

        for(String modId: moduleIds){
            Map<String, Object> validMap = new HashMap<String, Object>();
            validMap.put("parentId", modId);
            validMap.put("state", ConstantUtil.IS_VALID_Y);
            int count = sysModuleDao.selectSysModuleListCount(validMap);
            if(count>0){
                throw new ValidateException("您选择的模块下有子模块,暂无法删除!");
            }
        }
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("moduleIds",StringUtil.array2CharStr(moduleIds));
        sysModuleDao.deleteSysModule(map);
    }

    /**
     * 查询单个权限
     * @param moduleId
     * @throws DaoException
     */
    public Module selectSysModuleByModuleId(String moduleId) throws DaoException{
        SysModuleDao sysModuleDao = new SysModuleDaoImpl();
        return sysModuleDao.selectSysModuleByModuleId(moduleId);
    }

    /**
     * 查询单个权限
     * @param moduleCode
     * @throws DaoException
     */
    public Module selectSysModuleByModuleCode(String moduleCode) throws DaoException{
        SysModuleDao sysModuleDao = new SysModuleDaoImpl();
        return sysModuleDao.selectSysModuleByModuleCode(moduleCode);
    }

    /**
     * 查询所有权限集合
     * @param map
     * @throws DaoException
     */
    public List<Module> selectAllSysModuleList(Map<String,Object> map) throws DaoException {
        SysModuleDao sysModuleDao = new SysModuleDaoImpl();
        return sysModuleDao.selectAllSysModuleList(map);
    }

    /**
     * 分页查询权限集合
     * @param map
     * @throws DaoException
     */
    public List<Module> selectSysModulePageList(Map<String,Object> map) throws DaoException{
        SysModuleDao sysModuleDao = new SysModuleDaoImpl();
        return sysModuleDao.selectSysModuleList(map);
    }

    /**
     * 查询权限集合总数
     * @param map
     * @throws DaoException
     */
    public int selectSysModuleListCount(Map<String,Object> map) throws DaoException{
        SysModuleDao sysModuleDao = new SysModuleDaoImpl();
        return sysModuleDao.selectSysModuleListCount(map);
    }

    /**
     * 查询所有功能模块树形集合
     * @param sysCode 系统编号
     * @param parentId 上级模块ID
     * @return
     * @throws DaoException
     */
    public List<ModuleTreeVo> selectAllModuleTreeVoList(String sysCode,String parentId,String ... noModuleIds) throws DaoException, ValidateException {
        List<ModuleTreeVo> moduleTreeVos = null;
        Connection conn = null;

        if(StringUtil.isNull(parentId)){
            throw new ValidateException("parentId参数不能为空");
        }
        try {
            conn = new BaseDao().getConnection();
            Map<String,Object> map = new HashMap<String,Object>();
            map.put("parentId", parentId);

            if(noModuleIds!=null && noModuleIds.length>0){
                map.put("noModuleIds",StringUtil.array2CharStr(noModuleIds));
            }

            if(StringUtil.isNotNull(sysCode)){
                map.put("sysCode", sysCode);
            }
            moduleTreeVos = loopModuleTreeVoList(conn, map);
        } catch (Exception e) {
            throw new DaoException(e,e.getMessage());
        }finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return moduleTreeVos;
    }

    /**
     * 查询权限设置树形菜单集合 查数据库
     * @param sysCode
     * @param parentId
     * @return
     * @throws DaoException
     */
    public List<ModuleTreeVo> selectDistributeModuleTreeVoList(String sysCode,String parentId,String ... noModuleIds) throws DaoException {
        List<ModuleTreeVo> moduleTreeVos = null;
        Connection conn = new BaseDao().getConnection();
        try {
            Map<String,Object> map = new HashMap<String,Object>();
            map.put("defaultStatus", ConstantUtil.IS_VALID_N);
            map.put("showStatus", ConstantUtil.IS_VALID_Y);
            map.put("parentId", parentId);

            if(noModuleIds!=null && noModuleIds.length>0){
                map.put("noModuleIds",StringUtil.array2CharStr(noModuleIds));
            }

            if(StringUtil.isNotNull(sysCode)){
                map.put("sysCode", sysCode);
            }
            moduleTreeVos = loopModuleTreeVoList(conn, map);
        } catch (Exception e) {
            throw new DaoException(e,e.getMessage());
        }finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return moduleTreeVos;
    }


    /**
     * 递归查询树形菜单
     * @param conn
     * @param map
     * @return
     * @throws DaoException
     */
    private List<ModuleTreeVo> loopModuleTreeVoList(Connection conn,Map<String,Object> map) throws DaoException{
        List<ModuleTreeVo> moduleTreeVos = null;
        SysModuleDao sysModuleDao = new SysModuleDaoImpl();
        moduleTreeVos = sysModuleDao.selectAllModuleTreeVoList(conn,map);
        if(moduleTreeVos!=null && moduleTreeVos.size()>0){
            for(ModuleTreeVo moduleTreeVo : moduleTreeVos){
                map.put("parentId",moduleTreeVo.getId());
                moduleTreeVo.setChildren(loopModuleTreeVoList(conn,map));
            }
        }
        return moduleTreeVos;
    }


    /**
     * 查询用户功能模块树形集合
     * @param sysCode 系统编号
     * @param parentId 上级模块ID
     * @param busiIds 关联ID数组 如角色ID
     * @return
     * @throws DaoException
     * @throws ValidateException
     */
    public List<ModuleTreeVo> selectUserModuleTreeVoList(String sysCode,String parentId,String [] busiIds,String ... noModuleIds) throws DaoException, ValidateException{
        List<ModuleTreeVo> moduleTreeVos = null;
        if(busiIds==null || busiIds.length<=0){
            throw new ValidateException("busiIds参数不能为空");
        }

        Connection conn = new BaseDao().getConnection();
        try {
            Map<String,Object> map = new HashMap<String,Object>();
            map.put("busiIds",StringUtil.array2CharStr(busiIds));
            map.put("parentId",parentId);
            if(noModuleIds!=null && noModuleIds.length>0){
                map.put("noModuleIds",StringUtil.array2CharStr(noModuleIds));
            }

            if(StringUtil.isNotNull(sysCode)){
                map.put("sysCode", sysCode);
            }

            moduleTreeVos = loopUserModuleTreeVoList(conn, map);
        } catch (Exception e) {
            throw new DaoException(e,e.getMessage());
        }finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return moduleTreeVos;
    }

    /**
     * 递归查询用户树形菜单
     * @param conn
     * @param map
     * @return
     * @throws DaoException
     */
    private List<ModuleTreeVo> loopUserModuleTreeVoList(Connection conn,Map<String,Object> map) throws DaoException{
        List<ModuleTreeVo> moduleTreeVos = null;
        SysModuleDao sysModuleDao = new SysModuleDaoImpl();
        moduleTreeVos = sysModuleDao.selectUserModuleTreeVoList(conn,map);
        if(moduleTreeVos!=null && moduleTreeVos.size()>0){
            for(ModuleTreeVo moduleTreeVo : moduleTreeVos){
                map.put("parentId",moduleTreeVo.getId());
                moduleTreeVo.setChildren(loopUserModuleTreeVoList(conn,map));
            }
        }
        return moduleTreeVos;
    }

    /**
     * 查询特定页面的用户操作模块
     * @param sysCode 系统编号
     * @param parentModuleCode 上级模块code
     * @param busiIds 关联ID数组 如角色ID
     * @return
     * @throws DaoException
     * @throws ValidateException
     */
    public List<ModuleTreeVo> selectModuleTreeListByOnePage(String sysCode,String parentModuleCode,String [] busiIds,String ... noModuleIds) throws DaoException, ValidateException{
        List<ModuleTreeVo> moduleTreeVos = null;

        Connection conn = null;
        if(busiIds==null || busiIds.length<=0){
            throw new ValidateException("busiIds参数不能为空");
        }

        try {
            conn = new BaseDao().getConnection();
            Map<String,Object> map = new HashMap<String,Object>();
            map.put("busiIds",StringUtil.array2CharStr(busiIds));
            map.put("parentModuleCode",parentModuleCode);

            if(noModuleIds!=null && noModuleIds.length>0){
                map.put("noModuleIds",StringUtil.array2CharStr(noModuleIds));
            }

            if(StringUtil.isNotNull(sysCode)){
                map.put("sysCode", sysCode);
            }
            SysModuleDao sysModuleDao = new SysModuleDaoImpl();
            moduleTreeVos = sysModuleDao.selectUserModuleTreeVoList(conn,map);
        } catch (Exception e) {
            throw new DaoException(e,e.getMessage());
        }finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return moduleTreeVos;
    }
}
