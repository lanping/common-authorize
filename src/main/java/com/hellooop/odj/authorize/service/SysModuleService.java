package com.hellooop.odj.authorize.service;

import com.hellooop.odj.authorize.bean.Module;
import com.hellooop.odj.authorize.bean.po.SysModule;
import com.hellooop.odj.authorize.bean.vo.ModuleTreeVo;
import com.hellooop.odj.authorize.utils.DaoException;
import com.hellooop.odj.authorize.utils.ValidateException;

import java.util.List;
import java.util.Map;

/**
 * @author lanping
 * @version 1.0
 * @date 2019/12/7
 */
public interface SysModuleService {

    /**
     * 新增权限
     * @param sysModule
     * @throws DaoException
     */
    public void insertSysModule(SysModule sysModule) throws DaoException, ValidateException;

    /**
     * 修改权限
     * @param sysModule
     * @throws DaoException
     */
    public void updateSysModule(SysModule sysModule) throws DaoException, ValidateException;

    /**
     * 删除权限
     * @param moduleIds
     * @throws DaoException
     */
    public void deleteSysModule(String... moduleIds) throws DaoException, ValidateException;

    /**
     * 查询单个权限
     * @param moduleId
     * @throws DaoException
     */
    public Module selectSysModuleByModuleId(String moduleId) throws DaoException;


    /**
     * 查询单个权限
     * @param moduleCode
     * @throws DaoException
     */
    public Module selectSysModuleByModuleCode(String moduleCode) throws DaoException;

    /**
     * 查询所有权限集合
     * @param map
     * @throws DaoException
     */
    public List<Module> selectAllSysModuleList(Map<String, Object> map) throws DaoException;

    /**
     * 分页查询权限集合
     * @param map
     * @throws DaoException
     */
    public List<Module> selectSysModulePageList(Map<String, Object> map) throws DaoException;

    /**
     * 查询权限集合总数
     * @param map
     * @throws DaoException
     */
    public int selectSysModuleListCount(Map<String, Object> map) throws DaoException;

    /**
     * 查询所有功能模块树形集合
     * @param sysCode 系统编号
     * @param parentId 上级模块ID
     * @return
     * @throws DaoException
     */
    public List<ModuleTreeVo> selectAllModuleTreeVoList(String sysCode, String parentId,String ... noModuleIds)
            throws DaoException, ValidateException;

    /**
     * 查询权限设置树形菜单集合 查数据库
     * @param sysCode
     * @param parentId
     * @return
     * @throws DaoException
     */
    public List<ModuleTreeVo> selectDistributeModuleTreeVoList(String sysCode,String parentId,String ... noModuleIds) throws DaoException;


    /**
     * 查询用户菜单树形集合
     * @param sysCode 系统编号
     * @param parentId 上级编号 默认0
     * @param busiIds 关联ID 如角色ID
     * @return
     * @throws ValidateException
     */
    public List<ModuleTreeVo> selectUserModuleTreeVoList(String sysCode, String parentId, String [] busiIds,String ... noModuleIds)
            throws DaoException,ValidateException;

    /**
     * 查询特定页面的用户操作模块
     * @param sysCode 系统编号
     * @param parentModuleCode 上级模块code
     * @param busiIds 关联ID数组 如角色ID
     * @return
     * @throws DaoException
     * @throws ValidateException
     */
    public List<ModuleTreeVo> selectModuleTreeListByOnePage(String sysCode,String parentModuleCode,String [] busiIds,String ... noModuleIds)
            throws DaoException, ValidateException;
}
