package com.hellooop.odj.authorize.manager;

import com.alibaba.fastjson.JSONObject;
import com.hellooop.odj.authorize.bean.Module;
import com.hellooop.odj.authorize.bean.po.SysModule;
import com.hellooop.odj.authorize.bean.vo.ModuleTreeVo;
import com.hellooop.odj.authorize.bean.vo.ModuleType;
import com.hellooop.odj.authorize.dao.SysModuleDao;
import com.hellooop.odj.authorize.dao.impl.SysModuleDaoImpl;
import com.hellooop.odj.authorize.service.SysModuleService;
import com.hellooop.odj.authorize.service.impl.SysModuleServiceImpl;
import com.hellooop.odj.authorize.utils.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 系统模块基本操作类
 * @author lanping
 * @version 1.0
 * @date 2019/12/7
 */
public class ModuleBaseManager {

    /**
     * 新增权限
     * @param sysModule
     * @throws DaoException
     */
    public void addSysModule(SysModule sysModule) throws DaoException, ValidateException {
        SysModuleService sysModuleService = new SysModuleServiceImpl();
        sysModuleService.insertSysModule(sysModule);
    }

    /**
     * 修改权限
     * @param sysModule
     * @throws DaoException
     */
    public void updateSysModule(SysModule sysModule) throws DaoException, ValidateException {
        SysModuleService sysModuleService = new SysModuleServiceImpl();
        sysModuleService.updateSysModule(sysModule);
    }

    /**
     * 删除权限
     * @param moduleIds
     * @throws DaoException
     */
    public void deleteSysModule(String ... moduleIds) throws DaoException, ValidateException {
        SysModuleService sysModuleService = new SysModuleServiceImpl();
        sysModuleService.deleteSysModule(moduleIds);
    }

    /**
     * 查询单个权限
     * @param moduleId
     * @throws DaoException
     */
    public Module selectSysModuleByModuleId(String moduleId) throws DaoException{
        SysModuleService sysModuleService = new SysModuleServiceImpl();
        return sysModuleService.selectSysModuleByModuleId(moduleId);
    }

    /**
     * 查询单个权限
     * @param moduleCode
     * @throws DaoException
     */
    public Module selectSysModuleByModuleCode(String moduleCode) throws DaoException{
        SysModuleService sysModuleService = new SysModuleServiceImpl();
        return sysModuleService.selectSysModuleByModuleCode(moduleCode);
    }

    /**
     * 查询所有权限集合
     * @param map
     * @throws DaoException
     */
    public List<Module> selectAllSysModuleList(Map<String,Object> map) throws DaoException {
        SysModuleService sysModuleService = new SysModuleServiceImpl();
        return sysModuleService.selectAllSysModuleList(map);
    }

    /**
     * 查询模块类型集合
     * @throws DaoException
     */
    public List<ModuleType> selectModuleTypeList(){
        List<ModuleType> moduleTypes = new ArrayList<ModuleType>();
        for(ConstantUtil.moduleType type:ConstantUtil.moduleType.values()){
            ModuleType moduleType = new ModuleType();
            moduleType.setCode(type.getCode());
            moduleType.setName(type.getName());
            moduleTypes.add(moduleType);
        }
        return moduleTypes;
    }

    /**
     * 分页查询权限集合
     * @param map
     * @throws DaoException
     */
    public List<Module> selectSysModulePageList(Map<String,Object> map) throws DaoException{
        SysModuleService sysModuleService = new SysModuleServiceImpl();
        return sysModuleService.selectSysModulePageList(map);
    }

    /**
     * 查询权限集合总数
     * @param map
     * @throws DaoException
     */
    public int selectSysModuleListCount(Map<String,Object> map) throws DaoException{
        SysModuleService sysModuleService = new SysModuleServiceImpl();
        return sysModuleService.selectSysModuleListCount(map);
    }

    /**
     * 查询所有功能模块树形集合
     * @param sysCode 系统编号
     * @param parentId 上级模块ID
     * @return
     * @throws DaoException
     */
    public List<ModuleTreeVo> selectAllModuleTreeVoList(String sysCode,String rootId,String parentId,String ... noModuleIds) throws DaoException, ValidateException {

        List<ModuleTreeVo> moduleTreeVos = null;
        String key = sysCode+rootId+ ConstantUtil.REDIS_KEY_ALL_MODULE_MENUS_SUFFX;
        RedisTemplate redisTemplate = new RedisTemplate();
        boolean flag = redisTemplate.exists(key);
        if(flag == true){
            String json = redisTemplate.get(key);
            if(StringUtil.isNotNull(json)){
                //获取parentId下的集合
                moduleTreeVos = getModuleTreesByParent(JSONObject.parseArray(json, ModuleTreeVo.class),parentId);
            }
        }else{
            SysModuleService sysModuleService = new SysModuleServiceImpl();
            moduleTreeVos = sysModuleService.selectAllModuleTreeVoList(sysCode,parentId,noModuleIds);
        }
        return moduleTreeVos;
    }

    /**
     * 排序
     * @param moduleIds
     * @throws ValidateException
     * @throws DaoException
     */
    public void sortModules(String[] moduleIds) throws ValidateException, DaoException {
        if(moduleIds==null || moduleIds.length == 0){
            throw new ValidateException("模块参数错误！");
        }

        SysModuleDao sysModuleDao = new SysModuleDaoImpl();

        int rank = 1;
        for(String moduleId: moduleIds){
            SysModule sysModule = new SysModule();
            sysModule.setModuleId(moduleId);
            sysModule.setRank(rank);
            sysModuleDao.updateSysModule(sysModule);
            rank++;
        }
    }

    /**
     * 获取parentId下的children
     * @param moduleTreeVos
     * @param parentId
     */
    protected static List<ModuleTreeVo> getModuleTreesByParent(List<ModuleTreeVo> moduleTreeVos,String parentId) {
        List<ModuleTreeVo> moduleTreeVoList = new ArrayList<>();
        transModuleChildrenByParent(moduleTreeVos,parentId,moduleTreeVoList);
        return moduleTreeVoList;
    }

    private static void transModuleChildrenByParent(List<ModuleTreeVo> moduleTreeVos,
                                                    String parentId,List<ModuleTreeVo> moduleTreeVoList){
        if(moduleTreeVos!=null && moduleTreeVos.size()>0){
            for(ModuleTreeVo moduleTreeVo : moduleTreeVos){
                if(parentId.equals(moduleTreeVo.getParentId())){
                    if(moduleTreeVoList!=null){
                      moduleTreeVoList.add(moduleTreeVo);
                    }else{
                        moduleTreeVoList = new ArrayList<ModuleTreeVo>();
                        moduleTreeVoList.add(moduleTreeVo);
                    }
                }else{
                    transModuleChildrenByParent(moduleTreeVo.getChildren(),parentId,moduleTreeVoList);
                }
            }
        }
    }
}
