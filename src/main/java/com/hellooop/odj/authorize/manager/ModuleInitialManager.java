package com.hellooop.odj.authorize.manager;

import com.alibaba.fastjson.JSONArray;
import com.hellooop.odj.authorize.bean.vo.ModuleTreeVo;
import com.hellooop.odj.authorize.service.SysModuleService;
import com.hellooop.odj.authorize.service.impl.SysModuleServiceImpl;
import com.hellooop.odj.authorize.utils.ConstantUtil;
import com.hellooop.odj.authorize.utils.RedisTemplate;
import com.hellooop.odj.authorize.utils.StringUtil;

import java.util.List;

/**
 * 模块初始化操作
 * @author lanping
 * @version 1.0
 * @date 2019/12/7
 */
public class ModuleInitialManager {

    /**
     * 初始化用户模块缓存
     * @param sysCode 系统编号
     * @param rootId
     * @param parentId
     * @param excludeIds 不包括的业务ID数组（例如系统管理员ID）
     * @param busiIds	包括的业务ID数组 例如角色
     * @throws Exception
     */
    public void initUserModuleCache(String sysCode,String rootId,String parentId,String [] excludeIds,String [] busiIds,String ... noModuleIds) throws Exception {
        try{
            rootId = StringUtil.isNotNull(rootId)?rootId:ConstantUtil.DEFAULT_ROOT_PARANT_ID;
            SysModuleService sysModuleService = new SysModuleServiceImpl();
            RedisTemplate redisTemplate = new RedisTemplate();
            if(busiIds!=null && busiIds.length>0){
                for(String busiId:busiIds){
                    String key = sysCode+rootId+busiId+ConstantUtil.REDIS_KEY_USER_MODULES_SUFFX;
                    List<ModuleTreeVo> moduleTreeVos = null;
                    boolean flag = false;
                    if(excludeIds!=null && excludeIds.length>0){
                        for(String excludeId:excludeIds){
                            if(busiId.equals(excludeId)){
                                flag = true;
                                break;
                            }
                        }
                    }

                    //如果是排除在外的，则查询全部权限设置菜单
                    if(flag == true){
                        moduleTreeVos = sysModuleService.selectAllModuleTreeVoList(sysCode,parentId,noModuleIds);
                    }else{
                        moduleTreeVos = sysModuleService.selectUserModuleTreeVoList(sysCode,parentId,new String[]{busiId},noModuleIds);
                    }
                    if(moduleTreeVos!=null && moduleTreeVos.size()>0){
                        redisTemplate.set(key, JSONArray.toJSONString(moduleTreeVos));
                    }
                }
            }
        }catch (Exception e){
            throw new Exception(e.getMessage(),e);
        }
    }

    /**
     * 初始化系统所有模块缓存
     * @param sysCode 系统编号
     * @param rootId 上级ID
     * @param parentId
     * @param noModuleIds 不存在的ID
     * @throws Exception
     */
    public void initSysModuleCache(String sysCode,String rootId,String parentId,String ... noModuleIds) throws Exception {
        try{
            rootId = StringUtil.isNotNull(rootId)?rootId:ConstantUtil.DEFAULT_ROOT_PARANT_ID;

            SysModuleService sysModuleService = new SysModuleServiceImpl();
            RedisTemplate redisTemplate = new RedisTemplate();

            //查询所有的模块
            List<ModuleTreeVo> allModuleTreeVos = sysModuleService.selectAllModuleTreeVoList(sysCode,parentId,noModuleIds);
            if(allModuleTreeVos!=null && allModuleTreeVos.size()>0){
                redisTemplate.set(sysCode+rootId+ConstantUtil.REDIS_KEY_ALL_MODULE_MENUS_SUFFX, JSONArray.toJSONString(allModuleTreeVos));
            }
        }catch (Exception e){
            throw new Exception(e.getMessage(),e);
        }
    }

    /**
     * 初始化角色模块设置缓存
     * @param sysCode 系统编号
     * @param rootId 上级ID
     * @param noModuleIds
     * @throws Exception
     */
    public void initRoleModuleSetCache(String sysCode,String rootId,String parentId,String ... noModuleIds) throws Exception {
        try{
            rootId = StringUtil.isNotNull(rootId)?rootId:ConstantUtil.DEFAULT_ROOT_PARANT_ID;

            SysModuleService sysModuleService = new SysModuleServiceImpl();
            RedisTemplate redisTemplate = new RedisTemplate();

            //功能权限设置模块
            List<ModuleTreeVo> perModuleTreeVos = sysModuleService.selectDistributeModuleTreeVoList(sysCode,parentId,noModuleIds);
            if(perModuleTreeVos!=null && perModuleTreeVos.size()>0){
                redisTemplate.set(sysCode+rootId+ConstantUtil.REDIS_KEY_PERMISSION_MODULE_MENUS_SUFFX, JSONArray.toJSONString(perModuleTreeVos));
            }
        }catch (Exception e){
            throw new Exception(e.getMessage(),e);
        }
    }

    /**
     * 清除模块缓存
     * @param sysCode 系统编号
     * @param rootId
     * @param busiIds	包括的业务ID数组 例如角色
     * @throws Exception
     */
    public void clearUserModuleCache(String sysCode,String rootId,String ... busiIds) throws Exception {

        rootId = StringUtil.isNotNull(rootId)?rootId:ConstantUtil.DEFAULT_ROOT_PARANT_ID;

        RedisTemplate redisTemplate = new RedisTemplate();
        if(busiIds!=null && busiIds.length>0){
            for(String busiId: busiIds){
                String key = sysCode+rootId+busiId+ ConstantUtil.REDIS_KEY_USER_MODULES_SUFFX;
                redisTemplate.del(key);
            }
        }
    }

    /**
     * 清除系统所有模块缓存
     * @param sysCode 系统编号
     * @param rootId	上级ID
     * @throws Exception
     */
    public void clearSysModuleCache(String sysCode,String rootId) throws Exception {

        rootId = StringUtil.isNotNull(rootId)?rootId:ConstantUtil.DEFAULT_ROOT_PARANT_ID;

        RedisTemplate redisTemplate = new RedisTemplate();
        redisTemplate.del(sysCode+rootId+ConstantUtil.REDIS_KEY_ALL_MODULE_MENUS_SUFFX);
    }

    /**
     * 清除模块缓存
     * @param sysCode 系统编号
     * @param rootId	上级ID
     * @throws Exception
     */
    public void clearRoleModuleSetCache(String sysCode,String rootId) throws Exception {
        rootId = StringUtil.isNotNull(rootId)?rootId:ConstantUtil.DEFAULT_ROOT_PARANT_ID;

        RedisTemplate redisTemplate = new RedisTemplate();
        redisTemplate.del(sysCode+rootId+ConstantUtil.REDIS_KEY_PERMISSION_MODULE_MENUS_SUFFX);
    }

}
