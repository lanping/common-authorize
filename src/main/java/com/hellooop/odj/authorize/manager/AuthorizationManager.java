package com.hellooop.odj.authorize.manager;

import com.alibaba.fastjson.JSONObject;
import com.hellooop.odj.authorize.bean.Module;
import com.hellooop.odj.authorize.bean.po.SysModuleLink;
import com.hellooop.odj.authorize.bean.vo.ModuleTreeVo;
import com.hellooop.odj.authorize.dao.ITransaction;
import com.hellooop.odj.authorize.dao.SysModuleLinkDao;
import com.hellooop.odj.authorize.dao.Transaction;
import com.hellooop.odj.authorize.dao.impl.SysModuleLinkDaoImpl;
import com.hellooop.odj.authorize.service.SysModuleService;
import com.hellooop.odj.authorize.service.impl.SysModuleServiceImpl;
import com.hellooop.odj.authorize.utils.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * 权限管理器
 * @author lanping
 * @version 1.0
 * @date 2019-11-30 下午2:34:58
 */
public class AuthorizationManager {

	/**
	 * 分配权限
	 * @param busiId
	 * @param moduleIds
	 * @throws DaoException
	 */
	public void distributeModule(String busiId,String [] moduleIds) throws DaoException, ValidateException {

		SysModuleLinkDao sysModuleLinkDao = new SysModuleLinkDaoImpl();

		//删除已存在的权限
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("busiId",busiId);
		sysModuleLinkDao.deleteSysModuleLink(map);

		ITransaction tran = new Transaction();
		try{
			//写库
			for(String moduleId: moduleIds){
				SysModuleLink moduleLink = new SysModuleLink();
				moduleLink.setLinkId(UUID.randomUUID().toString());
				moduleLink.setBusiId(busiId);
				moduleLink.setModuleId(moduleId);
				moduleLink.setState(ConstantUtil.IS_VALID_Y);
				sysModuleLinkDao.insertSysModuleLink(moduleLink, tran);
			}
			tran.commit();
		}catch (Exception e){
			tran.rollback();
			throw new DaoException(e,e.getMessage());
		}finally {
			tran.close();
		}

	}

	/**
	 * 校验权限
	 * @param sysCode
	 * @param busiId
	 * @param moduleCode
	 * @return
	 * @throws DaoException
	 */
	public boolean validModuleByModuleCode(String sysCode,String rootId,String parentId,String busiId,String moduleCode,String ... noModuleIds) throws DaoException, ValidateException {
		ModuleVisitorManager moduleVisitorManager = new ModuleVisitorManager();
		List<ModuleTreeVo> moduleTreeVos = moduleVisitorManager.selectUserModuleTreeVoList(sysCode, rootId,parentId,busiId,noModuleIds);
		System.out.println(JSONObject.toJSONString(moduleTreeVos));
		Result result = new Result(false,"校验不通过");
		checkModuleTreesByCode(moduleTreeVos,moduleCode,result);
		return result.isSuccess();
	}

	/**
	 * 校验权限
	 * @param sysCode
	 * @param busiId
	 * @param moduleUrl
	 * @return
	 * @throws DaoException
	 */
	public boolean validModuleByModuleUrl(String sysCode,String rootId,String parentId,String busiId,String moduleUrl,String ... noModuleIds) throws DaoException, ValidateException {
		boolean flag = true;
		//查询是否是拦截权限
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("sysCode",sysCode);
		map.put("checkModuleUrl",moduleUrl);
		map.put("defaultStatus",ConstantUtil.IS_VALID_N);
		map.put("showStatus",ConstantUtil.IS_VALID_Y);
		map.put("state",ConstantUtil.IS_VALID_Y);
		ModuleBaseManager sysModuleManager=new ModuleBaseManager();
		List<Module> modules = sysModuleManager.selectAllSysModuleList(map);
		if(modules!=null && modules.size()>0){
			String moduleCode = modules.get(0).getModuleCode();

			ModuleVisitorManager moduleVisitorManager = new ModuleVisitorManager();
			List<ModuleTreeVo> moduleTreeVos = moduleVisitorManager.selectUserModuleTreeVoList(sysCode,rootId,parentId,busiId,noModuleIds);
			System.out.println(JSONObject.toJSONString(moduleTreeVos));
			Result result = new Result(false,"校验不通过");
			checkModuleTreesByCode(moduleTreeVos,moduleCode,result);
			flag = result.isSuccess();
		}
		return flag;
	}

	/**
	 * 判断是否在集合中
	 * @param moduleTreeVos
	 * @return
	 */
	private void checkModuleTreesByCode(List<ModuleTreeVo> moduleTreeVos,String moduleCode,Result result){
		if(result.isSuccess() == true){
			return;
		}
		if(moduleTreeVos!=null && moduleTreeVos.size()>0){
			for(ModuleTreeVo moduleTreeVo : moduleTreeVos){
				if(moduleCode.equals(moduleTreeVo.getField())){
					result.setSuccess(true);
					result.setMsg("验证通过");
					break;
				}
				if(moduleTreeVo.getChildren()!=null && moduleTreeVo.getChildren().size()>0){
					checkModuleTreesByCode(moduleTreeVo.getChildren(),moduleCode,result);
				}
			}
		}
	}

	/**
	 * 查询权限设置功能模块树形集合
	 * @param sysCode
	 * @param parentId
	 * @return
	 * @throws DaoException
	 */
	public List<ModuleTreeVo> selectDistributeModuleTreeVoList(String sysCode,String rootId,String parentId,String ... noModuleIds) throws DaoException{
		List<ModuleTreeVo> moduleTreeVos = null;

		String key = sysCode+rootId+ConstantUtil.REDIS_KEY_PERMISSION_MODULE_MENUS_SUFFX;
		RedisTemplate redisTemplate = new RedisTemplate();
		boolean flag = redisTemplate.exists(key);
		if(flag == true){
			String json = redisTemplate.get(key);
			if(StringUtil.isNotNull(json)){
				moduleTreeVos = ModuleBaseManager.getModuleTreesByParent(JSONObject.parseArray(json, ModuleTreeVo.class),parentId);
			}
		}else{
			SysModuleService sysModuleService = new SysModuleServiceImpl();
			moduleTreeVos =	sysModuleService.selectDistributeModuleTreeVoList(sysCode,parentId,noModuleIds);
		}
		return moduleTreeVos;
	}
}
