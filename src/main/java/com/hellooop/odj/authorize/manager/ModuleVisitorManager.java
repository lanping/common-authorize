package com.hellooop.odj.authorize.manager;

import com.alibaba.fastjson.JSONObject;
import com.hellooop.odj.authorize.bean.Module;
import com.hellooop.odj.authorize.bean.po.SysModuleLink;
import com.hellooop.odj.authorize.bean.vo.ModuleTreeVo;
import com.hellooop.odj.authorize.dao.SysModuleLinkDao;
import com.hellooop.odj.authorize.dao.impl.SysModuleLinkDaoImpl;
import com.hellooop.odj.authorize.service.SysModuleService;
import com.hellooop.odj.authorize.service.impl.SysModuleServiceImpl;
import com.hellooop.odj.authorize.utils.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 模块访问者操作类
 * @author lanping
 * @version 1.0
 * @date 2019/12/7
 */
public class ModuleVisitorManager {

    /**
     * 查询用户菜单树形集合
     * @param sysCode 系统编号
     * @param parentId 上级编号 默认0
     * @param busiId 关联ID 如角色ID
     * @return
     * @throws ValidateException
     */
    public List<ModuleTreeVo> selectUserModuleTreeVoList(String sysCode,String rootId, String parentId,String busiId,String ... noModuleIds) throws ValidateException, DaoException {
        if(StringUtil.isNull(busiId)){
            throw new ValidateException("busiId参数不能为空");
        }

        List<ModuleTreeVo> moduleTreeVos = null;
        String key = sysCode+rootId+busiId+ ConstantUtil.REDIS_KEY_USER_MODULES_SUFFX;
        RedisTemplate redisTemplate = new RedisTemplate();
        boolean flag = redisTemplate.exists(key);
        if(flag == true){
            String json = redisTemplate.get(key);
            if(StringUtil.isNotNull(json)){
                moduleTreeVos = ModuleBaseManager.getModuleTreesByParent(JSONObject.parseArray(json, ModuleTreeVo.class),parentId);
            }
        }else{
            SysModuleService sysModuleService = new SysModuleServiceImpl();
            moduleTreeVos = sysModuleService.selectUserModuleTreeVoList(sysCode,parentId,new String[]{busiId},noModuleIds);
        }
        return moduleTreeVos;
    }

    /**
     * 查询特定页面的用户操作模块
     * @param sysCode 系统编号
     * @param parentModuleCode 上级模块code
     * @param busiIds 关联ID数组 如角色ID
     * @return
     * @throws DaoException
     * @throws ValidateException
     */
    public List<ModuleTreeVo> selectModuleTreeListByOnePage(String sysCode,String rootId,
            String parentModuleCode,String [] busiIds,String ... noModuleIds) throws DaoException, ValidateException{
        List<ModuleTreeVo> moduleTreeVos = null;
        SysModuleService sysModuleService = new SysModuleServiceImpl();
        Module module = sysModuleService.selectSysModuleByModuleCode(parentModuleCode);
        if(module!=null && StringUtil.isNotNull(module.getModuleId())){
            String key = sysCode+rootId+busiIds[0]+ConstantUtil.REDIS_KEY_USER_MODULES_SUFFX;
            RedisTemplate redisTemplate = new RedisTemplate();
            boolean flag = redisTemplate.exists(key);
            if(flag == true){
                String json = redisTemplate.get(key);
                if(StringUtil.isNotNull(json)){
                    List<ModuleTreeVo> moduleTrees = JSONObject.parseArray(json, ModuleTreeVo.class);
                    moduleTreeVos = ModuleBaseManager.getModuleTreesByParent(moduleTrees,module.getModuleId());
                }
            }else{
                moduleTreeVos = sysModuleService.selectModuleTreeListByOnePage(sysCode,parentModuleCode,busiIds,noModuleIds);
            }
        }

        return moduleTreeVos;
    }

    /**
     * 显示功能菜单并且选中该角色具有的菜单
     * @param sysCode 系统编号
     * @param parentId 上级模块ID
     * @param busiId 关联ID 例如 roleId
     * @param adminFlag 是否是系统管理员
     * @return
     * @throws DaoException
     */
    public JSONObject selectModuleTreeByVisitor(String sysCode,String rootId,String parentId,String busiId,boolean adminFlag,String ... noModuleIds) throws DaoException {
        List<ModuleTreeVo> moduleTreeVos = null;
        JSONObject jsonObject = new JSONObject();

        String key = sysCode+rootId+ConstantUtil.REDIS_KEY_PERMISSION_MODULE_MENUS_SUFFX;
        RedisTemplate redisTemplate = new RedisTemplate();
        boolean flag = redisTemplate.exists(key);
        if(flag == true){
            String json = redisTemplate.get(key);
            if(StringUtil.isNotNull(json)){
                moduleTreeVos = ModuleBaseManager.getModuleTreesByParent(JSONObject.parseArray(json, ModuleTreeVo.class),parentId);
            }
        }else{
            SysModuleService sysModuleService = new SysModuleServiceImpl();
            moduleTreeVos =	sysModuleService.selectDistributeModuleTreeVoList(sysCode,parentId,noModuleIds);
        }

        SysModuleLinkDao sysModuleLinkDao = new SysModuleLinkDaoImpl();
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("busiId", busiId);
        map.put("state", ConstantUtil.IS_VALID_Y);
        List<SysModuleLink> sysModuleLinks = sysModuleLinkDao.selectAllSysModuleLinkList(map);
        if(sysModuleLinks!=null && sysModuleLinks.size()>0){
            int length = parseModuleTrees(moduleTreeVos, sysModuleLinks,adminFlag,null);
            if(sysModuleLinks.size() >= length){
                jsonObject.put("checkAll", true);
            }
        }
        jsonObject.put("moduleTreeVos", moduleTreeVos);
        return jsonObject;
    }

    /**
     * 转换
     * @param moduleTreeVos
     * @param sysModuleLinks
     * @param disabled 是否不可用
     * @return
     */
    private int parseModuleTrees(List<ModuleTreeVo> moduleTreeVos,List<SysModuleLink> sysModuleLinks,boolean disabled,ModuleTreeVo parentModule){
        int count = 0;
        int checkLength = 0;
        if(moduleTreeVos!=null && moduleTreeVos.size()>0){
            for(ModuleTreeVo moduleTreeVo : moduleTreeVos){
                checkLength++;
                for(SysModuleLink moduleLink : sysModuleLinks){
                    if(moduleLink.getModuleId().equals(moduleTreeVo.getId())){
                        moduleTreeVo.setChecked(true);
                        moduleTreeVo.setDisabled(disabled);
                        count++;
                        break;
                    }
                }
                if(moduleTreeVo.getChildren()!=null && moduleTreeVo.getChildren().size()>0){
                    moduleTreeVo.setChecked(false);
                    checkLength += parseModuleTrees(moduleTreeVo.getChildren(), sysModuleLinks,disabled,moduleTreeVo);
                }
            }
            if(count== moduleTreeVos.size()){
                if(parentModule!=null){
                    parentModule.setChecked(true);
                }
            }
        }
        return checkLength;
    }
}
