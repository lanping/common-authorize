package com.hellooop.odj.authorize;

import com.alibaba.fastjson.JSONObject;
import com.hellooop.odj.authorize.bean.vo.ModuleTreeVo;
import com.hellooop.odj.authorize.manager.AuthorizationManager;
import com.hellooop.odj.authorize.utils.DaoException;
import com.hellooop.odj.authorize.utils.ValidateException;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lanping
 * @version 1.0
 * @date 2019/12/2
 */
public class ModuleTest {

    @Test
    public void moduleOppTest(){

        AuthorizationManager authorizationManager = new AuthorizationManager();
        /*try {
            List<ModuleTreeVo> moduleTreeVos = authorizationManager.selectUserModuleTreeVoList(null,"0","0ca329b9-57a8-4d2a-96d4-359ee7673ed2");
            System.out.println(JSONObject.toJSONString(moduleTreeVos));
        } catch (DaoException e) {
            e.printStackTrace();
        } catch (ValidateException e) {
            e.printStackTrace();
        }*/

        try {
            boolean flag = authorizationManager.validModuleByModuleCode("","-1","-1",
                    "0ca329b9-57a8-4d2a-96d4-359ee7673ed2","log_manage_view");
            System.out.println(flag);
        } catch (DaoException e) {
            e.printStackTrace();
        } catch (ValidateException e) {
            e.printStackTrace();
        }
    }
}
